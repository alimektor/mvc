<?php
use Blog\User;

$errMsg = [
    "code" => 0
];
    try {
        if (!$_POST["surname"] or !$_POST["password"] or !$_POST["confirmPassword"] or !$_POST["name"] or $_POST["password"] != $_POST["confirmPassword"] ){
            if (!$_POST["name"]){
                throw new \Exception("Введите своё имя!");
            }

            if (!$_POST["surname"]) {
                throw new \Exception("Введите свою фамилию!");
            }
            if (!$_POST["email"]){
                throw new \Exception("Введите свой E-mail !");
            }
            if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                throw new \Exception("Введите правильно почтовый адрес!");
            }
            if (!$_POST["password"]) {
                throw new \Exception("Ведите пароль!");
            }
            if (!$_POST["confirmPassword"]){
                throw new \Exception("Введите пароль повторно!");
            }

            if ($_POST["password"] != $_POST["confirmPassword"]) {
                throw new \Exception("Пароли не совпадают!");
            }
        }
        $oUser = new User();
        $oUser->setEmail(@$_POST["email"]);
        $oUser->setPasswordHash(@$_POST["password"]);
        $oUser->setName(@$_POST["name"]);
        $oUser->setSurname(@$_POST['surname']);
        $oUser->save();
    } catch (Exception $exc) {
        $errMsg["code"] = 1;
        $errMsg["message"] = $exc->getMessage();
    }
echo json_encode($errMsg);