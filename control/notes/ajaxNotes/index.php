<?php
/**
 * Created by PhpStorm.
 * User: Artem-PC
 * Date: 26.06.2017
 * Time: 13:59
 */
use Blog\User;
$oUser = User::current();
$arNotes = $oUser->getNotes();
$errMsg = [
    "code"=> 0
];

try{
    if (isset($_POST["notes"])) {
        $oNotes = new Blog\Note();
        $oNotes->setUser($oUser);
        $oNotes->setNote($_POST["notes"]);
        $oNotes->sendNote();
    }
    else{
        throw new \Exception("Вы пытаетесь отправить пустое сообщение");
    }
}
catch (Exception $exc)
    {
    $errMsg["code"] = 1;
    $errMsg["message"] = $exc->getMessage();
}
echo json_encode($errMsg);