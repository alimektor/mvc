<?php
use Blog\User;

$oUser = User::current();
$arNotes = $oUser->getNotes();

\System\Page::instance()->setData("notes", $arNotes);
\System\Page::instance()->addFooterScript("/view/notes/notes.js");
\System\Page::instance()->setTitle("Записи");
\System\Page::instance()->display("notes/notes.php");
