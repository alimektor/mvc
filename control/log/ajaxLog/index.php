<?php
use Blog\User;

$errMsg = [
    "code" => 0
];
/**
 * Проверка заполнение формы
 *
 */
try {
    if (@$_POST["email"] && @$_POST["password"]) {

        $oUser = User::getIdByEmail($_POST["email"]);
        if ($oUser->verifyPassword($_POST["password"])) {
            $oUser->login();
        } else {
            throw new \Exception("Неправильное сочетание логин/пароль");
        }
    } else {
        throw new \Exception("Заполните все поля!");
    }
} catch
(Exception $exc) {
    $errMsg["code"] = 1;
    $errMsg["message"] = $exc->getMessage();
}
echo json_encode($errMsg);



