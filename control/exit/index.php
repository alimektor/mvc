<?php
/**
 * Created by PhpStorm.
 * User: Artem-PC
 * Date: 26.06.2017
 * Time: 20:39
 */
use Blog\User;
$oUser = User::current();
$oUser->logout();
System\Page::instance()->display("main.php");