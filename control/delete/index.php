<?php
/**
 * Created by PhpStorm.
 * User: Artem-PC
 * Date: 27.06.2017
 * Time: 14:37
 */
use Blog\User;
$oUser = User::current();
$oUser->delete();
$oUser->logout();
System\Page::instance()->display("main.php");
