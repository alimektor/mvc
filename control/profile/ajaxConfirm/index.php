<?php
use Blog\User;

$errMsg = [
    "code" => 0
];
$userId = $_POST["id"];
try {
    $oUser = User::current();
    $confirm = $oUser->confirmFriend($userId);
    if ($confirm == false) {
        throw new Exception("Непредвиденная ошибка!");
    }
} catch (Exception $exc) {
    $errMsg["code"] = 1;
    $errMsg["message"] = $exc->getMessage();
}
echo json_encode($errMsg);
?>