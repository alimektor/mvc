<?php
use Blog\User;

$errMsg = [
    "code" => 0
];
$userId = $_POST["id"];
//var_dump($userId);
try {
    $oUser = User::current();
    $add = $oUser->addFriend($userId);
    if ($add == false) {
        throw new Exception("Заявка уже отправлена!");
    }
} catch (Exception $exc) {
    $errMsg["code"] = 1;
    $errMsg["message"] = $exc->getMessage();
}
echo json_encode($errMsg);
?>