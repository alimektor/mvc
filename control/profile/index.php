<?php
System\Page::instance()->addFooterScript("/view/profile/profile.js");
use Blog\User;
use Blog\Information;

if ((@$_GET["id"] == "") || (@$_GET["id"] == $_SESSION["userID"])) {
    $oUser = User::current();
    $title = "Мой профиль";
    \System\Page::instance()->setFlag("user-btn", true);
    \System\Page::instance()->addFooterScript("/view/profile/confirm.js");
} else {
    $id = $_GET["id"];
    $oUser = new User($id);
    $fullName = $oUser->getFullName();
    $title = $fullName;
    \System\Page::instance()->addFooterScript("/view/profile/add.js");
}

$fullName = $oUser->getFullName();
$arNotes = $oUser->getNotes();
$oInf = new Information($oUser->getId());
$status = $oInf->getTextStatus();
\System\Page::instance()->setData("status",$status);
\System\Page::instance()->setData("information",$oInf);
$arFriends = $oUser->getFriends();
$arRequests = $oUser->getRequests();


\System\Page::instance()->setData("requests", $arRequests);
\System\Page::instance()->setData("friends", $arFriends);
\System\Page::instance()->setData("notes", $arNotes);
\System\Page::instance()->setData("username", $fullName);
\System\Page::instance()->addStyle("/view/profile/style.css");
\System\Page::instance()->setTitle($title);
\System\Page::instance()->display("profile/profile.php");
