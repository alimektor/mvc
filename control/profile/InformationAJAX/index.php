<?php
use Blog\User;
use Blog\Information;
$errMsg = [
    "code" => 0
];
try{
    $oUser = User::current();
    $oInf = new Information($oUser->getId());
    $oInf->setTextStatus(@$_POST["status"]);
    $oInf->setVk(@$_POST["vk"]);
    $oInf->setTwitter(@$_POST["twitter"]);
    $oInf->setSkype(@$_POST["skype"]);
    $oInf->save();

} catch(Exception $exc) {
    $errMsg["code"] = 1;
    $errMsg["message"] = $exc->getMessage();
}


echo json_encode($errMsg);