<?php

namespace System;

/**
 * Класс, на основе паттерна singleton, релизующий формирование и
 * отображение HTML страницы. Отвечает за подключение, шаблона сайта, стилей и скриптов.
 *
 * @author Николай Колосов <nk@webberry.ru>
 * @version 1.1.2-atv
 */
class Page
{

    /**
     * Объект текущего класса (реализация singleton)
     * @var Page
     */
    protected static $instance;

    /**
     * Заголовок страницы
     * @var string
     */
    protected $title;

    /**
     * Массив с путями (http путями) до стилей
     * которые надо подключить к странице
     * @var array
     */
    protected $arStyles = [];

    /**
     * Массив с путями (http путями) до js скриптов
     * которые надо подключить в head страницы
     * @var
     */
    protected $headScripts = [];

    /**
     * Массив с путями (http путями) до js скриптов
     * которые надо подключить в конце страницы
     * @var
     */
    protected $footerScripts = [];

    /**
     * Содержимое страницы (заключенное между header'ом и footer'ом)
     * @var string
     */
    protected $content;

    /**
     * Флаги страницы
     * @var array
     */
    protected $arFlags;
	
    /**
     * Флаги страницы
     * @var array
     */
    protected $arData;

    /**
     * Получить сущность
     * @return \System\Page
     */
    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Получить заголовок страницы
     * @return string Заголовок
     */
    function getTitle()
    {
        return $this->title;
    }

    /**
     * Задать заголовок страницы
     * @param string $title Заголовок
     */
    function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Добавить js скрипт в head страницы
     * @param string $path Относительный или абсолютный путь к скрипту
     */
    public function addHeadScript($path)
    {
        $webPath = self::getWebPath($path);

        if (!in_array($webPath, $this->headScripts) && !in_array($webPath, $this->footerScripts)) {
            $this->headScripts[] = $webPath;
        }
    }

    /**
     * Метод определения абсолютного WEB пути, например, /view/mypage/script.js,
     * по переданному относительному пути (script.js) и расположению скрипта, вызвавшего метод подключения (скрипта, стиля и т.д.)
     * @param string $path
     * @return string
     */
    protected static function getWebPath($path)
    {
        if (substr($path, 0, 1) == "/")
            $webPath = $path;
        else
            $webPath = dirname(str_replace($_SERVER["DOCUMENT_ROOT"], "", str_replace("\\", "/", debug_backtrace()[1]["file"]))) . "/" . $path;

        return $webPath;
    }

    /**
     * Добавить js скрипт в конец страницы
     * @param string $path Относительный или абсолютный путь к скрипту
     */
    public function addFooterScript($path)
    {
        $webPath = self::getWebPath($path);
        if (!in_array($webPath, $this->headScripts) && !in_array($webPath, $this->footerScripts)) {
            $this->footerScripts[] = $webPath;
        }
    }

    /**
     * Получить массив путей до js скриптов, которые следует подключить в конце страницы
     * @return array
     */
    public function getFooterScripts()
    {
        return $this->footerScripts;
    }

    /**
     * Получить массив путей до js скриптов, которые следует подключить в head страницы
     * @return array
     */
    public function getHeadScripts()
    {
        return $this->headScripts;
    }

    /**
     * Добавить css стиль, который следует подключить к страницк
     * @param string $path Относительный или абсолютный путь к стилю
     */
    public function addStyle($path) {
		$webPath = self::getWebPath($path);
		if (!in_array($webPath, $this->arStyles)) {
			$this->arStyles[] = $webPath;
		}
	}

	/**
     * Убрать стиль из списка подключаемых к странице
     * @param string $absolutePath
     */
    public function removeStyle($absolutePath)
    {
        foreach ($this->arStyles as $i => $path) {
            if ($path == $absolutePath) {
                unset($this->arStyles[$i]);
            }
        }
    }

    /**
     * Получить массив путей до css стилей, которые следует подключить в
     * @return array
     */
    public function getStyles()
    {
        return $this->arStyles;
    }

    /**
     * Получить основное содержимое страницы (между header и footer)
     * @return string
     */
    function getContent()
    {
        return $this->content;
    }

    /**
     * Задать основное содержимое страницы (между header и footer)
     * @param string $content
     */
    function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Отобразить страницу. Вместе c header, footer, всеми стилями и скриптами
     * @param string $pagePath Путь к шаблону view основного содержимого страницы
     * @param string $template Шаблон скайта - по умолчанию "default"
     */
    function display($pagePath = null, $template = "default", $mobile = ""){
		// Определям где мы нажодимся в мобильном приложении или в браузере
		$mobilePath = $this->checkMobile();
        if ($pagePath) {
            ob_start();
            require $_SERVER["DOCUMENT_ROOT"] ."/view/" . $pagePath;
            $this->setContent(ob_get_clean());
        }
        require $_SERVER["DOCUMENT_ROOT"] ."/view/templates/$template/template.php";
    }

    /**
     * Подключить к странице блок
     * @param string $block Полное название блока, например, Examples/Test
     * @param mixed $params Параметры, в произвольной форме, например ассоциативный массив
     * @param string $template Шаблон блока, по умолчанию default (default может вовсе отсутствовать)
     */
    function addBlock($block, $params = null, $template = "default"){
		// Определям где мы нажодимся в мобильном приложении или в браузере
		$mobilePath = $this->checkMobile();
		
        require $_SERVER["DOCUMENT_ROOT"]  ."/block/$block/main.php";
		//unset($params);
        $templatePath = $_SERVER["DOCUMENT_ROOT"] ."/block/$block/templates/$template/main.php";

        // Шаблон по умолчанию может вовсе отсутствовать
        if ($template == "default") {
            include $templatePath;
        } else {
            require $templatePath;
        }
    }
	
	// Определям где мы нажодимся в мобильном приложении или в браузере
	// Если мы в мобильном приложении, возвращаем дополнительный путь
	private function checkMobile(){
		$result = "";
		$check_page = explode("/", trim($_SERVER["REQUEST_URI"], "/"))[0];
		if($check_page == "atv_app"){
			$result = "/atv_app";
		}
		return $result;
	}

    /**
     * Задать флаг для страницы
     * @param string $flag Название флага
     * @param mixed $value Значение флага
     */
    public function setFlag($flag, $value = true){
        $this->arFlags[$flag] = $value;
    }

    /**
     * Получить флаг для станицы
     * @param string $flag Название флага
     * @return mixed Значение флага
     */
    public function getFlag($flag){
		if (isset($this->arFlags[$flag])) {
			return $this->arFlags[$flag];
		}
		else {
			return null;
		}
    }

    /**
     * Задать данные для страницы
     * @param string $item Название (ключ) элемента данных
     * @param mixed $value Задать значение элемента
     */
    public function setData($item, $value = true) {
		$this->arData[$item] = $value;
	}

	/**
     * Получить данные для станицы
     * @param string $item Название (ключ) элемента данных
     * @return mixed Значение элемента
     */
    public function getData($item) {
		if (isset($this->arData[$item])) {
			return $this->arData[$item];
		}
		else {
			return null;
		}
	}

}
