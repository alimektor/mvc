<?php

namespace System;

/**
 * Класс для сохранения, отображения, (обрезки) изображений
 *
 * @author Николай Колосов <nk@webberry.ru>
 */
class Image extends File {

	/**
	 * Широта
	 * @var float
	 */
	protected $latitude;

	/**
	 * Долгота
	 * @var float
	 */
	protected $longitude;

	/**
	 * Список расширений изображений
	 */
	const EXTENSIONS = ["jpeg", "jpg", "svg", "png", "gif", "ico", "bmp", "tiff"];

	/**
	 * Создать объект по uuid изображения (файла)
	 * @param int|null $uuid
	 */
	function __construct($uuid = null) {
		if ($uuid) {
			$constructSQL = "SELECT * FROM " . self::FILE_TABLE . " WHERE uuid = " . SQL::Value($uuid) . " LIMIT 1";
			$oRow = SQL::GetRow($constructSQL, true);
			if ($oRow) {
				$this->setUuid($oRow->uuid);
				$this->setEntity($oRow->entity);
				$this->setEntityId($oRow->entity_id);
				$this->setExtension($oRow->extension);
				$this->setName($oRow->name);
				$this->setTitle($oRow->title);
				$this->setDescription($oRow->description);
				$this->setTemp($oRow->temp);
				$this->setLatitude($oRow->latitude);
				$this->setLongitude($oRow->longitude);
			}
		}
	}

	/**
	 * Сохранить изображение на диск и в базу
	 * @param \System\File $file
	 */
	public function save() {
		parent::save();
		$this->saveCoordinates();
	}

	/**
	 * Сохранить координаты изображения в БД
	 */
	protected function saveCoordinates() {
		if ($this->getUuid()) {
			$sql = "UPDATE " . parent::FILE_TABLE . " " .
				"SET latitude = '" . (float) $this->getLatitude() . "', " .
				"longitude = '" . (float) $this->getLongitude() . "' " .
				"WHERE uuid = " . SQL::Value($this->getUuid()) . " LIMIT 1";
			SQL::Query($sql);
		}
	}

	/**
	 * Задать расширение файла. Например, "jpeg".
	 * @param string $extension
     * @throws \Exception
	 */
	function setExtension($extension) {
		if (!in_array($extension, Image::EXTENSIONS)) {
			throw new \Exception("Файл не является изображением");
		}

		$this->extension = $extension;
	}

	/**
	 * Получить широту
	 * @return float
	 */
	function getLatitude() {
		return (float) $this->latitude;
	}

	/**
	 * Получить долготу
	 * @return float
	 */
	function getLongitude() {
		return (float) $this->longitude;
	}

	/**
	 * Задать широту
	 * @param float $latitude
	 */
	function setLatitude($latitude) {
		$this->latitude = (float) $latitude;
	}

	/**
	 * Задать долготу
	 * @param float $longitude
	 */
	function setLongitude($longitude) {
		$this->longitude = (float) $longitude;
	}

	/**
	 * Запдать координаты
	 * @param float $latitude
	 * @param float $longitude
	 */
	function setCoordinates($latitude, $longitude) {
		$this->setLatitude($latitude);
		$this->setLongitude($longitude);
	}

	/**
	 * Получить координаты через запятую
	 * @return string
	 */
	function getCoordinates() {
		return $this->getLatitude() . "," . $this->getLongitude();
	}

	/**
	 * Задать временный путь к файлу.
	 * Это путь откуда надо скопировать файл в место постоянного хранения.
	 * Как правило это ["tmp_name"] из массива $_FILE при загрузке файла.
	 * @param string $tmpPath
	 */
	function setTmpPath($tmpPath) {
		$this->tmpPath = $tmpPath;

        // Получаем координаты из фотографии
        $exif = @exif_read_data($tmpPath);
        if (isset($exif["GPSLongitude"])) {
            $this->setLatitude(self::getGps($exif["GPSLongitude"]));}
        if (isset($exif["GPSLatitude"])) {
            $this->setLongitude(self::getGps($exif["GPSLatitude"]));
        }
	}

	/**
	 * Получить широту или долготу по EXIF данным GPSLongitude или GPSLatitude
	 * @param mixed $exifCoord
	 * @return float
	 */
	protected static function getGps($exifCoord) {
		//Pass in GPS.GPSLatitude or GPS.GPSLongitude or something in that format
		$degrees = count($exifCoord) > 0 ? self::gps2Num($exifCoord[0]) : 0;
		$minutes = count($exifCoord) > 1 ? self::gps2Num($exifCoord[1]) : 0;
		$seconds = count($exifCoord) > 2 ? self::gps2Num($exifCoord[2]) : 0;

		$flip = ($hemi == 'W' or $hemi == 'S') ? -1 : 1;
		return $flip * ($degrees + $minutes / 60 + $seconds / 3600);
	}

	/**
	 * Перевести координаты в число
	 * @param type $coordPart
	 * @return int
	 */
	protected static function gps2Num($coordPart) {
		$parts = explode('/', $coordPart);

		if (count($parts) <= 0)// jic
			return 0;
		if (count($parts) == 1)
			return $parts[0];
		return floatval($parts[0]) / floatval($parts[1]);
	}

	/**
	 * Сохранить файл изображения.
	 * @param string $tmpPath Временный путь к файлу (/tmp/sdfgafas), откуда надо скопировать файл.
	 * @param string $filename Имя файла ("Зеленый слоник.png").
	 * @param string $entity Сущность владельца файла
	 * @param int $entityId Идентификатор сущности владельца файла
	 * @param bool $temp Файл временный или нет
	 * @param string $description Описание файла
	 * @param string $name Название файла (Например, если нужно заменить "Зеленый слоник" (из оригинального названия), на "Зеленый слон").
	 * @param string $extension Название файла (Например, если нужно заменить "png" (из оригинального названия), на "jpeg").
     * @param int $maxWidth Максимальная ширина изображения.
     * @param int $maxHeight Максимальная высота изображения.
	 * @return File Объект вновь сохраненного файла.
	 */
	public static function saveFile($tmpPath, $filename = null, $entity = null, $entityId = null, $temp = false, $description = null, $name = null, $extension = null, $maxWidth = 1920, $maxHeight = 1080) {
		$oImage = new Image;
        if ($filename) {
            $oImage->setFilename($filename);
        }
		$oImage->setTmpPath($tmpPath);

		if ($entity)
			$oImage->setEntity($entity);

		if ($entityId)
			$oImage->setEntityId($entityId);

		if ($temp)
			$oImage->setTemp($temp);

		if ($description)
			$oImage->setDescription($description);

		if ($name)
			$oImage->setName($name);

		if ($extension)
			$oImage->setExtension($extension);
        $oImage->resize($maxWidth, $maxHeight);
		$oImage->save();
		return $oImage;
	}

	/**
	 * Добавить комментарий к текущему изображению
	 * @param \Atv\Comment\Comment $comment Комментарий. Если он отсутствует в БД - то будет сохранен.
	 */
	public function addComment(\Atv\Comment\Comment $comment) {
		if (!$this->getId()) {
			throw new \Exception("Текущий пост не сохранен в БД");
		}
		// Если добавленный комментарий не сохранен (отсутствует в БД), то сохраним его.
		if (!$comment->getId()) {
			$comment->save();
		}
		$sql = "REPLACE INTO w_image_has_comment (image_id, comment_id) VALUES (" . (int) $this->getId() . ", " . (int) $comment->getId() . ")";
		SQL::Query($sql);
	}

	/**
	 * Удалить комментарий у изображения и удалить совсем.
	 * @param \Atv\Comment\Comment $comment
	 */
	public function deleteComment(\Atv\Comment\Comment $comment) {
		$sql = "DELETE FROM w_image_has_comment WHERE image_id = " . (int) $this->getId() . " AND comment_id = " . (int) $comment->getId() . " LIMIT 1";
		$comment->delete();
	}

	/**
	 *
	 * @param \Atv\Comment\Comment $commentFrom
	 * @param int $count
	 * @return \Atv\Comment\Comment
	 */
	public function getComments(\Atv\Comment\Comment $commentFrom = null, $count = 25) {
		$arComments = [];
		if ($commentFrom) {
			$sqlFrom = "AND comment_id > " . (int) $commentFrom->getId();
		}
		$sql = "SELECT comment_id FROM w_image_has_comment WHERE image_id = " . (int) $this->getId() . " $sqlFrom LIMIT " . (int) $count;
		foreach (SQL::GetIDArray($sql, "comment_id") as $id) {
			$arComments[] = new \Atv\Comment\Comment($id);
		}
		return $arComments;
	}

    /**
     * Удалить текущий файл
     * @throws Exception
     */
    public function delete() {

        if (!$this->getUuid()) {
            return;
        }

        $url = $_SERVER["DOCUMENT_ROOT"]."/upload/".self::SUB_PATH."/".$this->getEntity();
        if ($this->getEntityId()) {
            $url .= "/".$this->getEntityId();
        }
        $url.="/".$this->getUuid();

        $deleteResult = array_map("unlink", glob($url."*.*"));

        if (file_exists($this->getPath()) && !empty($deleteResult)) {
            // Невозможно удалить файл
            throw new \Exception("Невозможно удалить файл из файловой системы");
        }

        SQL::Query("DELETE FROM " . self::FILE_TABLE . " WHERE uuid = '" . $this->getUuid() . "' LIMIT 1");

        // Файла не существует - все его поля пусты
        $this->setUuid(null);
        $this->setDescription(null);
        $this->extension = null;
        $this->setEntity(null);
        $this->setEntityId(null);
        $this->setTemp(false);
        $this->setName(null);
        $this->tmpPath = null;
    }

    /**
     * Получить URL к файлу
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getUrl($width = null, $height = null)
    {
        if (!$this->getUuid()) {
            return "";
        }

        $url = "/upload/".self::SUB_PATH."/".$this->getEntity();
        if ($this->getEntityId()) {
            $url .= "/".$this->getEntityId();
        }

        $origPath = $url."/".$this->getFilename();

        //Вычисляем недостающие значения
        if (!$width && $height) {
            $image_info = getimagesize($_SERVER["DOCUMENT_ROOT"].$origPath);
            $width = self::resizeWidthByHeight($image_info[0], $image_info[1], $height);
        }

        if (!$height && $width) {
            $image_info = getimagesize($_SERVER["DOCUMENT_ROOT"].$origPath);
            $height = self::resizeHeightByWidth($image_info[0], $image_info[1], $width);
        }

        if ($width && $height) {
            $newPath = $url."/".$this->getUuid()."_".$width."x".$height.".".$this->getExtension();
            if (!file_exists($newPath)) {
                if (copy($_SERVER["DOCUMENT_ROOT"].$origPath, $_SERVER["DOCUMENT_ROOT"].$newPath)) {
                    $oCopyImage = new self();
                    $oCopyImage->setTmpPath($_SERVER["DOCUMENT_ROOT"].$newPath);
                    $oCopyImage->resize($width, $height);
                    $url = $newPath;
                } else {
                    $url = $origPath;
                }
            } else {
                $url = $newPath;
            }
        } else {
            $url = $origPath;
        }

        return $url;
    }

    /**
     * Функция для скейла изображения
     * @param int $width Максимальная ширина изображения
     * @param int $height Максимальная высота изображения
     * @throws \Exception
     */
    public function resize($width = null, $height = null) {
        if ($width && $height) {
            if ($this->getUuid()) {
                $path = $this->getPath();
            } else {
                $path = $this->getTmpPath();
            }
            if (!$path) {
                throw new \Exception("Файла не существует");
            }

            $image_info = getimagesize($path);
            $imageType = $image_info[2]; //Определяем исходное расширение картинки
            if ($imageType == IMAGETYPE_JPEG) {
                $image = imagecreatefromjpeg($path);
            } elseif ($imageType == IMAGETYPE_GIF) {
                $image = imagecreatefromgif($path);
            } elseif ($imageType == IMAGETYPE_PNG) {
                $image = imagecreatefrompng($path);
            }

            $origWidth = $image_info[0];
            $origHeight = $image_info[1];

            if($origWidth > $width || $origHeight > $height) //вычисляем новые границы для картики
            {
                if ( $origWidth > $origHeight ) {
                    $resizeHeight = self::resizeHeightByWidth($origWidth, $origHeight, $width);
                    $resizeWidth  = $width;
                } else if( $origWidth < $origHeight ) {
                    $resizeWidth  = self::resizeWidthByHeight($origWidth, $origHeight, $height);
                    $resizeHeight = $height;
                }  else {
                    $resizeWidth = $width;
                    $resizeHeight = $height;
                }
            } else {
                $resizeWidth = $width;
                $resizeHeight = $height;
            }

            $new_image = imagecreatetruecolor($resizeWidth, $resizeHeight);
            imagecopyresampled($new_image, $image, 0, 0, 0, 0, $resizeWidth, $resizeHeight, $origWidth, $origHeight);
            $image = $new_image;
            // Сохраняем в лучшем виде
            if ($imageType == IMAGETYPE_JPEG) {
                imagejpeg($image, $path, 100);
            } elseif ($imageType == IMAGETYPE_GIF) {
                imagegif($image, $path);
            } elseif ($imageType == IMAGETYPE_PNG) {
                imagepng($image, $path, 0);
            }
        }
    }

    /**
     * Рассчитать высоту картинки для ресайза
     *
     * @param  int $width - Максимальная ширина изображения
     *
     * @return int Рассчитаная высота
     */
    private function resizeHeightByWidth($origWidth, $origHeight, $width)
    {
        return floor(($origHeight/$origWidth)*$width);
    }

    /**
     * Рассчитать ширину картинки для ресайза
     *
     * @param  int $height - Максимальная высота изображения
     *
     * @return int Рассчитаная ширина
     */
    private function resizeWidthByHeight($origWidth, $origHeight, $height)
    {
        return floor(($origWidth/$origHeight)*$height);
    }
}
