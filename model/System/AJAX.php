<?php

namespace System;

/**
 * Класс для упрощения создания AJAX обработчиков и приведения их к единому стандарту обмена.
 *
 * @package System
 * @author Николай Колосов <nk@webberry.ru>
 * @version 1.0.1-atv
 */
class AJAX {

	/**
	 * Метод печатает массив(для JS) с переданными данными в формате JSON и завершает выполнение скрипта
	 * @param mixed $data Даннык
	 * @param int $code Код возврата
	 * @param string $message Сообщение
	 */
	public static function Response($data = "", $code = 0, $message = "") {
		$arResponse = [
			"code" => $code,
			"message" => $message,
			"data" => $data,
		];
		print json_encode($arResponse);
		exit();
	}


	/**
	 * Получает параметры $_POST["PARAMETERS"] формате закодированной строки (как в GET запросе) или в формате массива
	 * и передает параметры пользовательской функции указанной в $_POST["ACTION"]
	 */
	public static function Init() {
		if (isset($_POST["PARAMETERS"]) && is_array($_POST["PARAMETERS"])) {
            $arParams = $_POST["PARAMETERS"];
        }
		else {
			if (isset($_POST["PARAMETERS"])) {
				parse_str($_POST["PARAMETERS"], $arParams);
			}
			else {
				$arParams = [];
			}
        }
		if (isset($_POST["ACTION"])) {
			call_user_func($_POST["ACTION"], $arParams);
		}
	}

}
