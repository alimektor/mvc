<?php

namespace System;

/**
 * Класс работы с файлами - сохранение, отображение и т.д.
 *
 * @author Николай Колосов <nk@webberry.ru>
 * @version 2.0.0-atv
 */
class File {

	/**
	 * Подраздел (относительно upload) в который будем складывать все файлы.
	 */
	const SUB_PATH = "files";

	/**
	 * Таблица с информацией о файлах
	 */
	const FILE_TABLE = "w_file";

	/**
	 * Права по умолчанию на создание вложенных директорий
	 */
	const CHMOD_DIR = 0755;

	/**
	 * Уникальный идентификатор файла
	 * @var string
	 */
	protected $uuid;

	/**
	 * Сущность, которой принадлежит файл. Например, пользователь.
	 * @var string|null
	 */
	protected $entity;

	/**
	 * Идентификатор сущности, которой принадлежит файл.
	 * @var int|null
	 */
	protected $entityId = null;

	/**
	 * расширение файла
	 * @var string
	 */
	protected $extension = null;

	/**
	 * Название файла
	 * @var string
	 */
	protected $name;

	/**
	 * описание файла
	 * @var string
	 */
	protected $description;

	/**
	 * Заголовок файла
	 * @var string
	 */
	protected $title;

	/**
	 * Флаг того, что файл временный
	 * @var bool
	 */
	protected $temp = false;

	/**
	 * Временный путь к файлу (на момент сохранения)
	 * @var string
	 */
	protected $tmpPath;

	/**
	 * Создать файл
	 * @param string $uuid
	 * @param string $entity
	 * @param int|string $entityId
	 * @param string $extension
	 * @param string $name
	 * @param string $description
	 * @param bool $temp
	 */
	function __construct($uuid = null) {
		if ($uuid) {
			$constructSQL = "SELECT * FROM " . self::FILE_TABLE . " WHERE uuid = " . SQL::Value($uuid) . " LIMIT 1";
			$oRow = SQL::GetRow($constructSQL, true);
			if ($oRow) {
				$this->setUuid($oRow->uuid);
				$this->setEntity($oRow->entity);
				$this->setEntityId($oRow->entity_id);
				$this->setExtension($oRow->extension);
				$this->setName($oRow->name);
				$this->setTitle($oRow->title);
				$this->setDescription($oRow->description);
				$this->setTemp($oRow->temp);
			}
		}
	}

	/**
	 * Сохранить файл.
	 * (в файловую систему и БД)
	 * @throws \Exception
	 */
    public function save() {
        if (!$this->getName()) throw new \Exception("Не указано название файла");

        $extension = SQL::Value($this->getExtension());
        $name = SQL::Value($this->getName());
        $title = SQL::Value($this->getTitle());
        $description = SQL::Value($this->getDescription());
        $temp = ($this->isTemp() ? 1 : "NULL");
        $entity = SQL::Value($this->getEntity());
        $entityId = SQL::Value($this->getEntityId());


        if ($this->getUuid()) {
            // Обновить информацию о файле
            $uuid = SQL::EscapeString($this->getUuid());
            $updateSQL = "UPDATE " . self::FILE_TABLE . " SET extension = $extension, name = $name, title = $title, description = $description, temp = $temp, entity = $entity, entity_id = $entityId WHERE uuid = '$uuid'";
            SQL::Query($updateSQL);
        } else {
            // Сохранить информацию о файле
            $uuid = SQL::EscapeString(File::generateUuid());
            $insertSQL = "INSERT INTO " . self::FILE_TABLE . " (uuid, extension, name, title, description, temp, entity, entity_id) VALUES ('$uuid', $extension, $name, $title, $description, $temp, $entity, $entityId)";
            SQL::Query($insertSQL);
            $this->setUuid($uuid);


            // Проверяем наличие директории для данного файла и если её нет - создаем, а если не смогли - бросаем
            if (!file_exists($this->getDir()) && !mkdir($this->getDir(), (int)File::CHMOD_DIR, true)) {
                throw new \Exception("Невозможно создать вложенную директорию при сохранении файла для сущности " . $this->getEntity() . " id " . $this->getEntityId());
            }

            if (!copy($this->getTmpPath(), $this->getDir() . $this->getUuid() . "." . $this->getExtension())) {
                SQL::Query("DELETE FROM " . self::FILE_TABLE . " WHERE uuid = '" . $this->getUuid() . "' LIMIT 1");
                throw new \Exception("Невозможно сохранить файл");
            }
        }
    }

	/**
	 * Удалить файл по uuid
	 * @param string $uuid
	 * @throws \Exception
	 */
	public static function deleteByUuid($uuid) {
		$oFile = FileDB::get($uuid);
		FileDB::delete($oFile);
	}

	/**
	 * Удалить временные файлы
	 * @param string $entity Для указанной сущности
	 * @param int $entityId Для указаной сущности и указанного id сущности
	 */
	public static function deleteTemp($entity = null, $entityId = null) {

		// Создаем запрос на получение временных файлов
		$getSql = "SELECT uuid FROM " . self::FILE_TABLE . " WHERE temp = 1 ";

		if ($entity) {
			$getSql .= " AND entity = " . SQL::Value($entity) . " ";
		}
		if ($entity && (int) $entityId) {
			$getSql .= " AND entity_id = " . (int) $entityId . " ";
		}

		// Удаляем файлы
		$arTempFiles = SQL::GetIDArray($getSql, "uuid");
		foreach ($arTempFiles as $uuid) {
			$oFile = new File($uuid);
			$oFile->delete();
		}
	}

	/**
	 * Удалить текущий файл
	 * @throws Exception
	 */
	public function delete() {

		if (!$this->getUuid()) {
			return;
		}

		if (file_exists($this->getPath()) && !unlink($this->getPath())) {
			// Невозможно удалить файл
			throw new \Exception("Невозможно удалить файл из файловой системы");
		}

		SQL::Query("DELETE FROM " . self::FILE_TABLE . " WHERE uuid = '" . $this->getUuid() . "' LIMIT 1");

		// Файла не существует - все его поля пусты
		$this->setUuid(null);
		$this->setDescription(null);
		$this->extension = null;
		$this->setEntity(null);
		$this->setEntityId(null);
		$this->setTemp(false);
		$this->setName(null);
		$this->setTmpPath(null);
	}

	/**
	 * Сгенерировать uuid
	 * @return string
	 */
	public static function generateUuid() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
	}

	/**
	 * Получить путь к директории куда будут складываться все файлы
	 * @return string
	 */
	protected static function getBasePath() {
		$baseDir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR . self::SUB_PATH . DIRECTORY_SEPARATOR;
		if (!is_dir($baseDir))
			mkdir($baseDir);

		return $baseDir;
	}

	/**
	 * Получить имя файла в файловой системе (uuid.extension)<br>
	 * ПОЛУЧАЕМОЕ ИМЯ ФАЙЛА НЕ СООТВЕТСТВУЕТ name.extension!<br>
	 * Это имя файла, хранимое в файловой системе!<br>
	 * Раблотает только для сохраненных файлов.<br>
	 * @return string
	 */
	protected function getFilename() {
		if (!$this->getUuid())
			return "";

		return $this->getUuid() . "." . $this->getExtension();
	}

	/**
	 * Получить полный путь к файлу в файловой системе
	 * @return string
	 */
	public function getPath() {
		if (!$this->getUuid())
			return "";

		return $this->getDir() . $this->getFilename();
	}

	/**
	 * Получить URL к файлу
	 * @return string
	 */
	public function getUrl() {
		if (!$this->getUuid())
			return "";


		$url = "/upload/" . self::SUB_PATH . "/" . $this->getEntity();
		if ($this->getEntityId()) {
			$url .= "/" . $this->getEntityId();
		}
		$url .= "/" . $this->getFilename();
		return $url;
	}

	/**
	 * Получить идентификатор файла
	 * @return string
	 */
	function getUuid() {
		return $this->uuid;
	}

	/**
	 * Получить расширение файла (без точки, например "png")
	 * @return string
	 */
	function getExtension() {
		return $this->extension;
	}

	/**
	 * Получить название файла (без расшрения, именно имя, например "Белая береза")
	 * @return string
	 */
	function getName() {
		return $this->name;
	}

	/**
	 * Получить описание файла (текст)
	 * @return string
	 */
	function getDescription() {
		return $this->description;
	}

	/**
	 * Получить заголовка файла (текст)
	 * @return string
	 */
	function getTitle() {
		return $this->title;
	}

	/**
	 * Задать уникальный идентификатор файла.
	 * @param string $uuid
	 */
	function setUuid($uuid) {
		$this->uuid = $uuid;
	}

	/**
	 * Задать расширение файла. Например, "jpeg".
	 * @param string $extension
	 */
	function setExtension($extension) {
		$this->extension = $extension;
	}

	/**
	 * Задать название файла. Например, "Красивый закат".
	 * @param string $name
	 */
	function setName($name) {
		$this->name = trim($name);
	}

	/**
	 * Задать описание файла.
	 * @param string $description
	 */
	function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * Задать заголовка файла.
	 * @param string $title
	 */
	function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Получить временный путь к файлу.
	 * Это путь откуда надо скопировать файл в место постоянного хранения.
	 * Как правило это ["tmp_name"] из массива $_FILE при загрузке файла.
	 * @return string
	 */
	function getTmpPath() {
		return $this->tmpPath;
	}

	/**
	 * Задать временный путь к файлу.
	 * Это путь откуда надо скопировать файл в место постоянного хранения.
	 * Как правило это ["tmp_name"] из массива $_FILE при загрузке файла.
	 * @param string $tmpPath
	 */
	function setTmpPath($tmpPath) {
		$this->tmpPath = $tmpPath;
	}

	/**
	 * Задать имя файла (оригинальное имя файла), например "Белая береза.jpeg",
	 * Далее это имя разделется на Name и Extension.
	 * @param string $filename
	 */
	public function setFilename($filename) {
//		setlocale(LC_ALL, 'ru_RU.utf8');
		$pathParts = pathinfo($filename);
		$extension = $pathParts['extension'];
		$name = $pathParts['filename'];
		$this->setExtension($extension);
		$this->setName($name);
	}

	/**
	 * Получить сущность владельца файла (например пользователь)
	 * @return string Сущность владельца файла
	 */
	function getEntity() {
		if (!$this->entity) {
			return null;
		}
		return $this->entity;
	}

	/**
	 * Получить идентификатор сущности владельца файла (напрмер id пользователя)
	 * @return int
	 */
	function getEntityId() {
		if (!$this->entityId) {
			return null;
		}
		return $this->entityId;
	}

	/**
	 * Узнать - временный файл или нет
	 * @return bool
	 */
	function isTemp() {
		return (bool) $this->temp;
	}

	/**
	 * Задать сущность владельца файла
	 * @param string $entity
	 */
	function setEntity($entity) {
		$entity = trim($entity);
		if (!$entity)
			$entity = null;

		$this->entity = $entity;
	}

	/**
	 * Задать идентификатор сущности владельца файла
	 * @param int $entityId Идентификатор сущности владельца файла
	 */
	function setEntityId($entityId) {
		$entityId = (int) $entityId;
		if (!$entityId)
			$entityId = null;

		$this->entityId = $entityId;
	}

	/**
	 * Указать, что файл является временным
	 * @param bool $temp Флаг - временный файл или нет
	 */
	function setTemp($temp) {
		$this->temp = (bool) $temp;
	}

	/**
	 * Получить директорию в файловой системе, в которой лежит файл
	 * @return string
	 */
	public function getDir() {
		$dirname = self::getBasePath();
		if ($this->getEntity()) {
			$dirname .= "/" . $this->getEntity();
		}
		if ($this->getEntity() && $this->getEntityId()) {
			$dirname .= "/" . $this->getEntityId();
		}
		$dirname .= "/";

		return $dirname;
	}

	/**
	 * Отправляет файл пользователю и ЗАВЕРШАЕТ выполнение скрипта.
	 * Использовать ДО ВЫВОДА ЧЕГО ЛИБО НА СТРАНИЦУ ПОЛЬЗОВАТЕЛЮ;
	 * Использовать так:
	 * <pre>
	 * <?php
	 * 	use Gam\combeep\File;
	 * 	$oFile = new File($uuid);
	 * 	$oFile->returnFile();
	 * ?>
	 * </pre>
	 * @throws \Exception
	 */
	public function returnFile() {
		if (!$this->getUuid())
			throw new \Exception("Такого файла не существует");

		if (file_exists($this->getPath())) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="' . $this->getName() . "." . $this->getExtension() . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($this->getPath()));
			readfile($this->getPath());
			exit;
		}
	}

	/**
	 * Сохранить файл.
	 * @param string $tmpPath Временный путь к файлу (/tmp/sdfgafas), откуда надо скопировать файл.
	 * @param string $filename Имя файла ("Зеленый слоник.png").
	 * @param string $entity Сущность владельца файла
	 * @param int $entityId Идентификатор сущности владельца файла
	 * @param bool $temp Файл временный или нет
	 * @param string $description Описание файла
	 * @param string $name Название файла (Например, если нужно заменить "Зеленый слоник" (из оригинального названия), на "Зеленый слон").
	 * @param string $extension Название файла (Например, если нужно заменить "png" (из оригинального названия), на "jpeg").
	 * @return \Gam\combeep\File Объект вновь сохраненного файла.
	 */
	public static function saveFile($tmpPath, $filename, $entity = null, $entityId = null, $temp = false, $description = null, $name = null, $extension = null) {
		$oFile = new File;
		$oFile->setFilename($filename);
		$oFile->setTmpPath($tmpPath);

		if ($entity)
			$oFile->setEntity($entity);

		if ($entityId)
			$oFile->setEntityId($entityId);

		if ($temp)
			$oFile->setTemp($temp);

		if ($description)
			$oFile->setDescription($description);

		if ($name)
			$oFile->setName($name);

		if ($extension)
			$oFile->setExtension($extension);

		FileDB::save($oFile);
		return $oFile;
	}

	public static function getFiles($entity = null, $entityId = null, $name = null){
		$arResult = [];
		if($entity){
			$entitySQL = " AND entity = ".SQL::Value($entity);
		}
		if($entityId){
			$entityIdSQL = " AND entity_id = ".SQL::Value($entityId);
		}
		if($name){
			$nameSQL = " AND name = ".SQL::Value($name);
		}
		
		$selectSQL = "SELECT uuid FROM w_file WHERE 1 $entitySQL $entityIdSQL $nameSQL";
		$arIds = SQL::GetIDArray($selectSQL, "uuid");
		foreach($arIds as $id){
			$arResult[] = new File($id);
		}
		return $arResult;
	}
}
