<?php
/**
 * Created by PhpStorm.
 * User: Crazyfen
 * Date: 011, 11.01.2017
 * Time: 16:49
 */

namespace System;


class Tools
{
    /**
     * Функция для вывода разных строк в зависимости от числа
     *
     * @param $n
     * @param $titles
     *
     * @return mixed
     */
    public static function endingFromNumber($n, $titles) {
        $cases = [2, 0, 1, 1, 1, 2];

        return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
    }

    /**
     * Отправляет серверу сокетов, что пользователю нужно обновить уведомления
     * @param User $user
     *
     * @throws \Exception
     */
    public static function sendNotificationEvent(User $user) {
        if (!$user || !$user->getId()) {
            throw new \Exception("Пользователь, не пользователь");
        }

        $arData = [
            "type" => "notification",
            "data" => [
                "user" => $user->getId(),
            ],
        ];

        $jsonData = json_encode($arData);

        $fp = stream_socket_client("tcp://127.0.0.1:8001", $errno, $errstr, 30); // открываем соединение с сокет сервером
        if (!$fp) {
            throw new \Exception("Не открылся сокет, ошибка $errno");
        } else {
            fwrite($fp, $jsonData); // и пишем в него массив параметров
            fclose($fp);
        }
    }
}