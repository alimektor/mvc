<?php

namespace System;

/**
 * Description of MailTemplate
 *
 * @author Николай Колосов <nk@webberry.ru>
 * @version 1.0.0-atv
 */
class MailTemplate {

	use TMailTemplateDB;

	/**
	 * Идентификатор шаблона письма
	 * @var int
	 */
	protected $id;

	/**
	 * Символьный код (название) шаблона письма
	 * @var string
	 */
	protected $alias;

	/**
	 * Тема письма
	 * @var string
	 */
	protected $subject;

	/**
	 * e-mail отправителя письма
	 * @var string
	 */
	protected $from;

	/**
	 * Перечисленные через запятую  e-mail'ы получателей
	 * @var type 
	 */
	protected $to;

	/**
	 * Перечисленные через запятую  e-mail'ы получателей в копии
	 * @var type 
	 */
	protected $copy;

	/**
	 * Перечисленные через запятую  e-mail'ы получателей в скрытой копии
	 * @var type 
	 */
	protected $copyHidden;

	/**
	 * Тип письма (html или text)
	 * @var string
	 */
	protected $type;

	/**
	 * Текст письма
	 * @var string
	 */
	protected $message;

	/**
	 * Описание почтового шаблона
	 * @var string 
	 */
	protected $description;

	/**
	 * Массив со значениями, подставляемые в шаблон
	 * @var string[]
	 */
	protected $arValues = [];

	/**
	 * Создать/получить новый почтовый шаблон
	 * @param string $alias Символьный код, название шаблона
	 * @param array $arValues Массив со значениями, подставляемые в шаблон<br>
	 * (в шаблоне #somthing#, в массиве ["somthing" => "какое то значение"], заменит #somthing# на "какое то значение")
	 */
	function __construct($alias = null, $arValues = []) {
		$this->arValues = $arValues;
		$this->dbGet($alias);
	}

	/**
	 * Заменить блоки #something# на значения из массива типа ["something" => 42]
	 * @param string $string
	 * @param string $arValues
	 * @return string
	 */
	protected static function setValues($string, $arValues) {
		foreach ($arValues as $key => $value) {
			$string = str_replace("#" . $key . "#", $value, $string);
		}
		return $string;
	}

	/**
	 * Получить идентификатор почтового шаблона
	 * @return int
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Получить  символьный код (название) почтового шаблона
	 * @return string
	 */
	function getAlias() {
		return $this->alias;
	}

	/**
	 * Получить тему письма
	 * @return string
	 */
	function getSubject() {
		return $this->subject;
	}

	/**
	 * Получить e-mail отправителя
	 * @return string
	 */
	function getFrom() {
		return $this->from;
	}

	/**
	 * Получить получателей письма (указываются через запятую) в формате <b>test@example.com</b> или в формате <b>"Дядя Петя <test@example.com>"</b>
	 * @return string
	 */
	function getTo() {
		return $this->to;
	}

	/**
	 * Получить получателей (в копии) письма (указываются через запятую) в формате <b>test@example.com</b> или в формате <b>"Дядя Петя <test@example.com>"</b>
	 * @return string
	 */
	function getCopy() {
		return $this->copy;
	}

	/**
	 * Получить получателей (в копии) письма (указываются через запятую) в формате <b>test@example.com</b> или в формате <b>"Дядя Петя <test@example.com>"</b>
	 * @return string
	 */
	function getCopyHidden() {
		return $this->copyHidden;
	}

	/**
	 * Получить тип письма (текстовое или html)
	 * @return string "text" или "html"
	 */
	function getType() {
		return $this->type;
	}

	/**
	 * Задать тип письма - текстовое или html
	 * @param string $type "text" или "html". По умолчанию "text".
	 */
	function setType($type) {
		if ($type == "html") {
			$this->type = $type;
		} else {
			$this->type = "text";
		}
	}

	/**
	 * Получить текст письма
	 * @return string
	 */
	function getMessage() {
		return $this->message;
	}

	/**
	 * Задать идентификатор почтового шаблона
	 * @param int $id
	 */
	function setId($id) {
		$this->id = $id;
	}

	/**
	 * Задать символьный код (название) почтового шаблона.
	 * @param string $alias
	 */
	function setAlias($alias) {
		$this->alias = $alias;
	}

	/**
	 * Задать заголовок письма
	 * @param type $subject
	 */
	function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * Задать отправителя в формате https://www.w3.org/Protocols/rfc822/#z10
	 * @param string $from
	 */
	function setFrom($from) {
		$this->from = $from;
	}

	/**
	 * Задать получателей письма в формате https://www.w3.org/Protocols/rfc822/#z10
	 * @param string $to
	 */
	function setTo($to) {
		$this->to = $to;
	}

	/**
	 * Задать получателей письма в копии в формате https://www.w3.org/Protocols/rfc822/#z10
	 * @param string $copy
	 */
	function setCopy($copy) {
		$this->copy = $copy;
	}

	/**
	 * Задать получателей письма в скрытой копии в формате https://www.w3.org/Protocols/rfc822/#z10
	 * @param string $CopyHidden
	 */
	function setCopyHidden($CopyHidden) {
		$this->copyHidden = $CopyHidden;
	}

	/**
	 * Задать текст письма
	 * @param string $message
	 */
	function setMessage($message) {
		$this->message = $message;
	}

	/**
	 * Получить массив значений, которые подставляются в шаблон
	 * @return string[]
	 */
	function getArValues() {
		return $this->arValues;
	}

	/**
	 * Задать массив со значениями, подставляемые в шаблон
	 * @param string[] $arValues
	 */
	function setArValues($arValues) {
		$this->arValues = (array) $arValues;
	}

	/**
	 * Получить описание почтового шаблона
	 * @return string
	 */
	function getDescription() {
		return $this->description;
	}

	/**
	 * Задать описание почтового шаблона
	 * @param string $description
	 */
	function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Сформировать и получить объект письма (подставив значения из $arValues в нужные области)
	 * Полученное письмо можно отправить методом ->send();
	 * @return \System\Mail
	 */
	function getMail() {
		$mail = new Mail;
		$arValues = $this->arValues;

		$mail->setSubject(self::setValues($this->subject, $arValues));
		$mail->setFrom(self::setValues($this->from, $arValues));
		$mail->setMessage(self::setValues($this->message, $arValues));

		foreach (explode(",", $this->to) as $emailTo) {
			$mail->addTo(self::setValues($emailTo, $arValues));
		}
		foreach (explode(",", $this->copy) as $emailCopy) {
			$mail->addCopy(self::setValues($emailCopy, $arValues));
		}
		foreach (explode(",", $this->copyHidden) as $emailCopyHidden) {
			$mail->addCopyHidden(self::setValues($emailCopyHidden, $arValues));
		}

		return $mail;
	}

}
