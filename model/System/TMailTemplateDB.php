<?php

namespace System;

/**
 * Description of MailTemplate
 *
 * @author Николай Колосов <nk@webberry.ru>
 */
use System\SQL;

trait TMailTemplateDB {

	protected function dbGet($alias) {
		$sql = "SELECT * FROM w_mail_template WHERE alias = " . SQL::Value($alias) . " LIMIT 1";
		$oRow = SQL::GetRow($sql, true);
		$this->id = (int) $oRow->id;
		$this->alias = $oRow->alias;
		$this->to = $oRow->to;
		$this->from = $oRow->from;
		$this->type = $oRow->type;
		$this->description = $oRow->description;
		$this->subject = $oRow->subject;
		$this->copy = $oRow->copy;
		$this->copyHidden = $oRow->copy_hidden;
		$this->message = $oRow->message;
	}

	public function save() {
		$baseSql = "SET alias = " . SQL::Value($this->getAlias()) . ", " .
			"`type` = " . SQL::Value($this->getType()) . ", " .
			"`subject` = " . SQL::Value($this->getSubject()) . ", " .
			"`from` = " . SQL::Value($this->getFrom()) . ", " .
			"`to` = " . SQL::Value($this->getTo()) . ", " .
			"`copy` = " . SQL::Value($this->getCopy()) . ", " .
			"`copy_hidden`  = " . SQL::Value($this->getCopyHidden()) . ", " .
			"`message` = " . SQL::Value($this->getMessage()) . ", " .
			"`description` = " . SQL::Value($this->getDescription());

		if ($this->getId()) {
			$sql = "UPDATE w_mail_template $baseSql WHERE id = " . (int) $this->getId();
			SQL::Query($sql);
		} else {
			$sql = "INSERT INTO w_mail_template $baseSql";
			$id = null;
			SQL::Query($sql, $id);
			$this->setId((int) $id);
		}
	}

}
