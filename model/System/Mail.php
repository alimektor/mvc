<?php

namespace System;

/**
 * Класс для создания и отправки писем.
 * Поддерживает text, html и вложения.
 *
 * @author Николай Колосов <nk@webberry.ru>
 * @version 1.0.0-atv
 */
class Mail {

	/**
	 * Адрес отправителя
	 * @var string
	 */
	protected $from;

	/**
	 * Адреса получателей письма
	 * @var array
	 */
	protected $arTo = [];

	/**
	 * Адреса получателей в копии
	 * @var string[]
	 */
	protected $arCopy = [];

	/**
	 * Адреса получателей в скрытой копии
	 * @var string[]
	 */
	protected $arCopyHidden = [];

	/**
	 * Тема письма
	 * @var string
	 */
	protected $subject = "Без темы";

	/**
	 * Сообщение письма
	 * @var string
	 */
	protected $message = "";

	/**
	 * Тип письма text или html
	 * @var string
	 */
	protected $type = "text";

	/**
	 * Кодировка письма
	 * @var string
	 */
	protected $charset = "utf-8";

	/**
	 * Массив с путями (в файловой системе) к прикреленным письмам
	 * @var string[]
	 */
	protected $arAttachments = [];

	/**
	 * Конец строки в заголовках письма
	 * @var string
	 */
	protected static $eol = "\r\n";

	/**
	 * Разделитель между заголовками (общим заголовком, заголовком сообщения, заголовками файлов)
	 * @var string
	 */
	protected $separator;

	/**
	 * Создать новое письмо
	 */
	public function __construct() {
		$this->separator = md5(time());
	}

	/**
	 * Получить отправителя письма
	 * @return string
	 */
	function getFrom() {
		return $this->from;
	}

	/**
	 * Задать отправителя письма
	 * @param string $email
	 */
	function setFrom($email) {
		$this->from = $email;
	}

	/**
	 * Задать единственного получателя письма (чтобы задать несколько получателей нужно использовать <b>addTo()</b>)
	 * @param string $email
	 */
	function setTo($email) {
		$this->arTo = [$email];
	}	
	
	/**
	 * Добавить получателя данного письма
	 * @param string $email
	 */
	function addTo($email) {
		$this->arTo[] = $email;
		$this->arTo = array_unique($this->arTo);
	}

	/**
	 * Получить массив получателей данного письма
	 * @return string
	 */
	function getTo() {
		return $this->arTo;
	}

	/**
	 * Добавить получателя данного письма в копии
	 * @param string$email
	 */
	function addCopy($email) {
		$this->arCopy[] = $email;
		$this->arCopy = array_unique($this->arCopy);
	}

	/**
	 * Получить массив получателей данного письма в копии
	 * @return string[]
	 */
	function getCopy() {
		return $this->arCopy;
	}

	/**
	 * Добавить получателя данного письма в скрытой копии
	 * @param string $email
	 */
	function addCopyHidden($email) {
		$this->arCopyHidden[] = $email;
		$this->arCopyHidden = array_unique($this->arCopyHidden);
	}

	/**
	 * Получить массив получателей данного письма в скрытой копии
	 * @return string[]
	 */
	function getCopyHidden() {
		return $this->arCopyHidden;
	}

	/**
	 * Получить сообщение (текст) письма
	 * @return string
	 */
	function getMessage() {
		return $this->message;
	}

	/**
	 * Задать сообщение (текст) письма
	 * @param string $message
	 */
	function setMessage($message) {
		$this->message = $message;
	}

	/**
	 * Получить тип письма (текстовое или html)
	 * @return string "text" или "html"
	 */
	function getType() {
		return $this->type;
	}

	/**
	 * Задать тип письма - текстовое или html
	 * @param string $type "text" или "html". По умолчанию "text".
	 */
	function setType($type) {
		if ($type == "html") {
			$this->type = $type;
		} else {
			$this->type = "text";
		}
	}

	/**
	 * Задать кодировку письма
	 * @param string $charset
	 */
	function setCharset($charset) {
		$this->charset = $charset;
	}

	/**
	 * Получить кодировку письма
	 * @return string 
	 */
	function getCharset() {
		return $this->charset;
	}

	/**
	 * Получить тему письма
	 * @return string
	 */
	function getSubject() {
		return $this->subject;
	}

	/**
	 * Установить тему письма
	 * @param string $subject
	 */
	function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * Путь в файловой системе до прикрепляемого файла
	 * @param string $file
	 */
	function addAttachment($file) {
		$this->arAttachments[] = $file;
		$this->arAttachments = array_unique($this->arAttachments);
	}

	/**
	 * Получить списк прикреленных файлов (массив путей в файловой системе)
	 * @return string[]
	 */
	function getAttachments() {
		return $this->arAttachments;
	}

	/**
	 * Получить заголовки письма
	 * @return string
	 */
	protected function getHeadets() {

		// main header (multipart mandatory)
		$headers = "From: " . $this->getFrom() . self::$eol;
		$headers .= "MIME-Version: 1.0" . self::$eol;
		$headers .= "Reply-To: " . $this->getFrom() . self::$eol;
		// Копия
		if ($this->getCopy()) {
			$headers .= "Cc: " . implode(", ", $this->getCopy()) . self::$eol;
		}
		// Скрытая копия
		if ($this->getCopyHidden()) {
			$headers .= "Bcc: " . implode(", ", $this->getCopyHidden()) . self::$eol;
		}
		$headers .= 'X-Mailer: PHP/' . phpversion() . self::$eol;
		$headers .= "Content-Type: multipart / mixed; boundary = \"" . $this->separator . "\"" . self::$eol;
		$headers .= "Content-Transfer-Encoding: 7bit" . self::$eol . self::$eol;

		return $headers;
	}

	/**
	 * Получить содержимое письма (Сообщение и вложения)
	 * @return string
	 */
	protected function getBody() {
		// message
		$body = "--" . $this->separator . self::$eol;
		$body .= "Content-type: text/" . ($this->getType() == "html" ? "html" : "plain") . "; charset=" . $this->getCharset() . self::$eol;
		$body .= "Content-Transfer-Encoding: 7bit" . self::$eol. self::$eol;
		$body .= $this->getMessage() . self::$eol . self::$eol;
		// Файлы
		foreach ($this->getAttachments() as $attachment) {
			$content = chunk_split(base64_encode(file_get_contents($attachment)));
			$body .= "--" . $this->separator . self::$eol;
			$body .= "Content-Type: application/octet-stream; name=\"" . basename($attachment) . "\"" . self::$eol;
			$body .= "Content-Transfer-Encoding: base64" . self::$eol;
			$body .= "Content-Disposition: attachment; filename=\"" . basename($attachment) . "\"" . self::$eol . self::$eol;
			$body .= $content . self::$eol . self::$eol;
		}
		$body .= "--" . $this->separator . "--";
		return $body;
	}

	/**
	 * Отправить письмо
	 * @return bool true - отправлено, false - не отправлено
	 */
	function send() {

		// если в поле from указано имя и email
		$mailFrom = self::getStringBetween($this->getFrom(), "<", ">");
		// если в поле from указан только email
		if (!$mailFrom) {
			$mailFrom = $this->getFrom();
		}

		return mail(implode(", ", $this->getTo()), $this->getSubject(), $this->getBody(), $this->getHeadets(), "-f$mailFrom");
	}

	/**
	 * Получить строку между строками $start и $end
	 * @param string $string Строка из которой надо получить подстроку
	 * @param string $start Подстрока - от которой надо выбрать
	 * @param string $end Подстрока - до которой надо выбрать
	 * @return string Результат - строка между $start и $end
	 */
	protected static function getStringBetween($string, $start, $end) {
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0)
			return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

}
