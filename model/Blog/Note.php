<?php

namespace Blog;
use System\SQL;

class Note
{

    /**
     * Константа TABLE - название таблицы в базе данных (notes)
     */
    const TABLE = "notes";

    /**
     * Идентификатор записи
     * @var int
     */
    protected $id;

    /**
     * Пользователь
     * @var \Blog\User
     */
    protected  $user;

    /**
     * Время публикации записи
     * @var string
     */
    protected $time;

    /**
     * Запись пользователя
     * @var string
     */
    protected $note;

    /**
     * Задать свойство: "Идентификатор записи"
     * @param int $id - Идентификатор записи
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Задать свойство: "Запись пользователя"
     * @param string $note - Запись пользователя
     */
    public function setNote(string $note)
    {
        $this->note = $note;
    }

    /**
     * Задать свойство: "Время публикации записи"
     * @param string $time - Время публикации записи
     */
    public function setTime(string $time)
    {
        $this->time = $time;
    }

    /**
     * Задать пользователя
     * @param User $user - Пользователь
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Получить свойство: "Идентификатор записи"
     * @return int - Идентификатор записи
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить свойство: "Запись пользователя"
     * @return string - Запись пользователя
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Получить свойство: "Время публикации записи"
     * @return string - Время публикации записи
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Получить пользователя
     * @return object User - Пользователь
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Конструктор класса "Запись"
     * @param int|null $id - Идентификатор записи
     */
    function __construct($id = null)
    {
        if ((int) $id) {
            $sql = "SELECT * FROM" . " " . self::TABLE . " WHERE id = " . $id;
            $arRow = SQL::GetRow($sql);
            $this->id = (int)$arRow["id"];
            $this->time = (string)$arRow["time"];
            $this->note = (string)$arRow["note"];
            $this->user = new User($arRow["user_id"]);
        }
    }

    /**
     * Отправить запись
     */
    public function sendNote()
    {
        $userId = (int) $this->getUser()->getId();
        $note = SQL::Value($this->note);
        SQL::Query("INSERT INTO" . " " . self::TABLE . " (`user_id`, `note`) VALUES ($userId, $note)");
    }
}