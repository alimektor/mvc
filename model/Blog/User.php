<?php

namespace Blog;

use System\SQL;

class User
{
    /**
     * Константа TABLE - название таблицы в базе данных (users)
     */
    const TABLE = "users";

    /**
     * Идентификатор пользователя
     * @var int
     */
    protected $id;

    /**
     * Почтовый адрес пользователя
     * @var string
     */
    protected $email;

    /**
     * Зашифрованный пароль пользователя
     * @var string
     */
    protected $passwordHash;

    /**
     * Имя пользователя
     * @var string
     */
    protected $name;

    /**
     * Фамилия пользователя
     * @var string
     */
    protected $surname;

    /**
     * Конструктор пользователя.
     * @param $id int Идентификатор пользователя
     */
    function __construct($id = null)
    {
        if ((int)$id) {
            $sql = "SELECT * FROM " . self::TABLE . " WHERE `id` = '" . (int)$id . "' LIMIT 1";
            $arRow = SQL::GetRow($sql);
            $this->id = (int)$arRow["id"];
            $this->email = (string)$arRow["email"];
            $this->name = (string)$arRow["name"];
            $this->surname = (string)$arRow["surname"];
            $this->passwordHash = (string)$arRow["password"];
        }
    }

    /**
     * Задать свойство: "Идентификатор пользователя"
     * @param int $id - Идентификатор пользователя
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Задать свойство: "Email пользователя"
     * @param string $email - Email пользователя
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Задать свойство "Зашифрованный пароль пользователя" (Пароль пользователя и зашифровать его)
     * @param string $password
     */
    public function setPasswordHash($password)
    {
        $this->passwordHash = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Задать свойство: "Имя пользователя"
     * @param mixed $name - Имя пользователя
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Задать свойство: "Фамилия пользователя"
     * @param string $surname - Фамилия пользователя
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * Получить свойство: "Идентификатор пользователя"
     * @return int - Идентификатор пользователя
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить свойство: "Email пользователя"
     * @return string - Email пользователя
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Получить свойство: "Зашифрованный пароль пользователя"
     * @return string - Зашифрованный пароль пользователя
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * Получить свойство: "Имя пользователя"
     * @return string - Имя пользователя
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Получить свойство: "Фамилия пользователя"
     * @return string - Фамилия пользователя
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Сохраняем пользователя в базе данных
     * @throws \Exception - Исключения ошибок при регистрации
     */
    public function save()
    {
        if (!$this->getEmail()) {
            throw new \Exception("Не указан почтовый адрес!");
        }

        if ((!$this->getPasswordHash()) || ($this->getPasswordHash() == "")) {
            throw new \Exception("Не указан пароль или поля пароля пусто!");
        }

        $email = SQL::Value($this->getEmail());

        $passwordHash = SQL::Value($this->getPasswordHash());
        $name = SQL::Value($this->getName());
        $surname = SQL::Value($this->getSurname());
        if ((int)$this->getId()) {
            $sql = "UPDATE " . self::TABLE . " SET
             `email` = $email,
             `password` = $passwordHash,
             `name` = $name,
             `surname` = $surname
             WHERE id = " . (int)$this->getId();
            SQL::Query($sql);
        } elseif (self::emailExists($this->getEmail())) {
                throw new \Exception("Логин уже существует!");
            } else {
                $sql = "INSERT INTO " . self::TABLE . " (`email`, `password`, `name`, `surname`) VALUES ($email, $passwordHash, $name, $surname)";
                $id = null;
                SQL::Query($sql, $id);
                $this->setId((int)$id);
            }
    }

    /**
     * Проверка почтового адреса на существование в базе данных
     * @param string $email - Введённый почтовый адрес
     * @return bool - true, если существует
     */
    public static function emailExists($email)
    {
        $selectEmail = "SELECT count(*) as cnt FROM " . self::TABLE . " WHERE `email` = " . SQL::Value($email);
        $num = (int)SQL::GetValue($selectEmail, "cnt");
        if ($num > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Залогиниться под текущим пользователям
     */
    public function login()
    {
        if ($this->getId()) {
            $_SESSION["userID"] = $this->getId();
        }
    }

    /**
     * Проверяет входящий пароль с паролем из базы данных
     * @param $password - Входящий пароль
     * @return bool - true, если успешная проверка
     */
    public function verifyPassword($password)
    {
        if (!$password) {
            return false;
        }

        if (password_verify($password, $this->getPasswordHash())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получить текущего пользователя
     * @return \Blog\User
     */
    public static function current()
    {
        return new User($_SESSION["userID"]);
    }

    /**
     * Получить нового пользователя по ID
     * @param string $email - Электронный почтовый адрес для проверки
     * @return User - Новый пользователь
     */
    public static function getIdByEmail($email)
    {
        $sql = "SELECT id FROM " . self::TABLE . " WHERE `email` = " . SQL::Value($email) . " LIMIT 1";
        $userID = SQL::GetValue($sql);
        return new User($userID);
    }

    /**
     * Получить записи пользователя
     * @return array - Массив записей пользователя
     */
    public function getNotes()
    {
        $arNotes = [];
        $sql = "SELECT `id` FROM " . Note::TABLE . " WHERE `user_id` = " . (int)$this->getId() . " ORDER BY `time` DESC";
        $arNotesId = SQL::GetIDArray($sql, "id");
        foreach ($arNotesId as $id) {
            $arNotes[] = new Note($id);
        }
        return $arNotes;
    }

    /**
     * Получить полное имя пользователя
     * @return string - Полное имя пользователя
     */
    public function getFullName()
    {
        return $this->getName() . " " . $this->getSurname();
    }

// ---------------------------------------------------

    /**
     * @param $id int - Идентификатор пользователя
     * @return bool - true, если заявка отправлена
     */
    public function addFriend($id)
    {
        $userId1 = (int)$this->getId();
        $oUser2 = new User($id);
        $userId2 = (int)$oUser2->getId();
        $checkSQL = "SELECT count(*) as cnt FROM friends
        WHERE (user1_id = '$userId1' and user2_id = '$userId2') or (user2_id = '$userId1' and user1_id = '$userId2')";
        $num = (int)SQL::GetValue($checkSQL, "cnt");
        if ($num > 0) {
            $checkRequest = "SELECT count(*) as cnt FROM friends WHERE user1_id = $userId1";
            $numRequest = SQL::GetValue($checkRequest, "cnt");
            if ($numRequest > 0) {
                return false;
            } else {
                $this->confirmFriend($userId2);
                return true;
            }
        } else {
            $insertSQL = "INSERT INTO friends (user1_id, user2_id) VALUES ('$userId1', '$userId2')";
            SQL::Query($insertSQL);
            return true;
        }
    }

    /**
     * Получить друзей пользователя
     * @return array - Возрашает массив друзей пользователя
     */
    public function getFriends() {
        $id = SQL::Value((int)$this->getId());
        $sql = "SELECT user2_id AS id FROM friends
                WHERE user1_id = $id AND conf = 1
                UNION
                SELECT user1_id AS id FROM friends
                WHERE user2_id = $id AND conf = 1";
        $arId = SQL::GetIDArray($sql);
        $arFriends = [];
        foreach ($arId as $idFriend) {
            $oUserFriend = new User($idFriend);
            $arFriends[$idFriend] = $oUserFriend;
        }
        return $arFriends;
    }

    /**
     * Получает всех пользователей, которые отправили заявку в друзья
     * @return array(User) - массив пользователей, отправивших заявку
     */
    public function getRequests() {
        $id = SQL::Value((int)$this->getId());
        $sql = "SELECT user1_id AS id FROM friends
                WHERE user2_id = $id AND conf IS NULL";
        $arId = SQL::GetIDArray($sql);
        $arRequests = [];
        foreach ($arId as $idRequest) {
            $oUserFriend = new User($idRequest);
            $arRequests[$idRequest] = $oUserFriend;
        }
        return $arRequests;
    }

    /**
     * Подтверждает заявку в друзья
     * @param $id - Идентификатор пользователя, заявку которого нужно подтвердить
     * @return bool - true, если заявка принята успешно
     */
    public function confirmFriend($id)
    {
        $oRequest = new User($id);
        $userIdRequest = $oRequest->getId();
        $userIdCurrent = $this->getId();
        $sql = "UPDATE friends SET conf = 1
WHERE user1_id = $userIdCurrent AND user2_id = $userIdRequest";
        $res = SQL::Query($sql);
        if ($res == true) {
            $sql = "UPDATE friends SET conf = 1
WHERE user1_id = $userIdRequest AND user2_id = $userIdCurrent";
            $res = SQL::Query($sql);
            if ($res == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * Очищает сессию пользователя
     */
    public function logout(){
        unset($_SESSION["userID"]);
    }

    /**
     * Удаления пользователя
     */
    public function delete(){
        $sql = "DELETE FROM `users` WHERE `id` = ". $this->getId();
        $del = SQL::Query($sql);
    }
}