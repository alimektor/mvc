<?php
/**
 * Created by PhpStorm.
 * User: Artem-PC
 * Date: 28.06.2017
 * Time: 13:56
 */

namespace Blog;

use System\SQL;
use Blog\User;
class Information
{
    const TABLE = "user_information";
    protected $id_user;
    protected $id_user_locate;
    protected $vk;
    protected $twitter;
    protected $skype;
    protected $text_status;

    function __construct($id_user = null)
    {
        $this->id_user = (int) $id_user;
        if ((int)$id_user) {
            $sql = "SELECT * FROM ".self::TABLE. " WHERE `id_user` = " .(int)$id_user. " LIMIT 1";
            $arRow = SQL::GetRow($sql);
            //$this->id_user = (int)$arRow["id_user"];
            $this->text_status = (string)$arRow["text_status"];
            $this->vk = (string) $arRow["vk"];
            $this->twitter = (string) $arRow["twitter"];
            $this->skype = (string) $arRow["skype"];
        }
    }

    /**
     * @return string
     */
    public function getVk()
    {
        return $this->vk;
    }

    /**
     * @param string $vk
     */
    public function setVk($vk)
    {
        $this->vk = $vk;
    }

    /**
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param string $skype
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
    }

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param int $id_user
     */
    public function setUser(User $user)
    {
         $this->id_user = $user->getId();
    }

    /**
     * @return string
     */
    public function getTextStatus()
    {
        return $this->text_status;
    }

    /**
     * @param string $text_status
     */
    public function setTextStatus($text_status)
    {
        $this->text_status = $text_status;
    }

    public function save(){
        $text_status = SQL::Value($this->getTextStatus());
        $vk = SQL::Value($this->getVk());
        $twitter = SQL::Value($this->getTwitter());
        $skype = SQL::Value($this->getSkype());
        $userId = (int)$this->getIdUser();
        if (!(int) $this->getIdUser()){
            throw new \Exception("Не заданидентификатор пользователя");
        }
        else{
            $sql = "REPLACE INTO ".self::TABLE."(id_user,`vk`,`twitter`,`skype`,`text_status`) VALUES($userId,$vk,$twitter,$skype,$text_status)";
            $id_user = null;
            SQL::Query($sql,$id_user);
            $this->id_user = (int)$id_user;
        }
    }
}