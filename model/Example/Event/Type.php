<?php

namespace Example\Event;

use System\SQL;

class Type {

	/**
	 * Константа - название таблицы в MySQL
	 */
	const W_EVENT_TYPE_TABLE = "w_event_type";

	/**
	 * Идентификатор
	 * @var int
	 */
	protected $id;

	/**
	 * Название типа
	 * @var string
	 */
	protected $name;

	/**
	 * Краткое описание типа
	 * @var string
	 */
	protected $alias;

	/**
	 * Конструктор класса Type
	 * @param int $id Идентификатор
	 */
	public function __construct($id = null) {
		// Проверка указания всех аргументов
		if (!$id) {
			return;
		}

		// Создаем SQL Запрос на получение данных для объекта
		$constructSQL = "SELECT * FROM " . self::W_EVENT_TYPE_TABLE . " WHERE id = '$id' LIMIT 1";
		$arRow = SQL::GetRow($constructSQL);

		// Заполнение свойств
		$this->id = (int) $arRow["id"];
		$this->name = (string) $arRow["name"];
		$this->alias = (string) $arRow["alias"];
	}

	/**
	 * Задать свойство: "Идентификатор"
	 * @param int $id Идентификатор
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * Задать свойство: "Название типа"
	 * @param string $name Название типа
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Задать свойство: "Краткое описание типа"
	 * @param string $alias Краткое описание типа
	 */
	public function setAlias($alias) {
		$this->alias = $alias;
	}

	/**
	 * Получить свойство: "Идентификатор"
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Получить свойство: "Название типа"
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Получить свойство: "Краткое описание типа"
	 * @return string
	 */
	public function getAlias() {
		return $this->alias;
	}

	/**
	 * Метод сохранения объекта в базу данных
	 */
	public function save() {
		$name = SQL::Value($this->getName());
		$alias = SQL::Value($this->getAlias());

		$baseSql = "`name` = $name, `alias` = $alias";

		if ($this->getId()) {
			$updateSql = "UPDATE " .self::W_EVENT_TYPE_TABLE. " SET $baseSql WHERE id = " . SQL::Value($this->getId()) . " LIMIT 1";
			SQL::Query($updateSql);
		} else {
			$insertSql = "INSERT INTO " .self::W_EVENT_TYPE_TABLE. " SET $baseSql";
			$id = null;
			SQL::Query($insertSql, $id);
			$this->id = (int) $id;
		}
	}

	/**
	 * Метод удаления объекта из базы данных
	 */
	public function delete() {
		$deleteSQL = "DELETE FROM " . self::W_EVENT_TYPE_TABLE . " WHERE `id` = '" . (int) $this->getId() . "' LIMIT 1";
		SQL::Query($deleteSQL);
	}

	/**
	 * Получить все объекты из базы данных в виде массива
	 * @return self[]
	 */
	public static function getAll() {
		// Создаем SQL Запрос на получение данных для объекта
		$getSQL = "SELECT id FROM " . self::W_EVENT_TYPE_TABLE;
		$arRows = SQL::QueryToArray($getSQL);

		foreach ($arRows as $arRow) {
			$arResult[] = new self($arRow["id"]);
		}
		return $arResult;
	}

	/**
	 * Получить объект из базы по краткому описанию
	 * @param string $alias - краткое описание типа
	 * @return \self
	 */
	public static function getByAlias($alias){
		$getSQL = "SELECT id FROM " . self::W_EVENT_TYPE_TABLE . " WHERE alias = ". SQL::EscapeString($alias);
		return new self(SQL::GetValue($getSQL));
	}
}
