<?php
date_default_timezone_set('Europe/Moscow');
require_once $_SERVER["DOCUMENT_ROOT"] . "/system/init.php";



route($_SERVER["DOCUMENT_ROOT"], $_GET["path"]);

/**
 * Подключить контроллер по переданному URL и DOCUMENT_ROOT
 * @param string $root DOCUMENT_ROOT сайта
 * @param string $path Url - переданный в GET запросе
 */
function route($root, $path)
{

    $controller = "$root/control/$path/index.php";

    // Если контроллер не существует физически на диске
    if (!file_exists($controller)) {
        $controller = "$root/control/404.php";
        \System\Page::instance()->setFlag("404");
        header("HTTP/1.0 404 Not Found");
    } else {
        checkAccess($path);
    }
    require_once $controller;
}

/**
 * Проверка прав доступа пользователя (авторизованного или нет) к разделам сайта
 * @param string $path Переданный URL
 * @return bool
 */
function checkAccess($path)
{
    return;
}
