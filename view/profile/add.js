$(document).ready(function () {
    $(".add-friends").click(function () {
        var userId = $("#userId").val();
        $.ajax({
            url: '/profile/ajaxAddFriend/',
            type: 'POST',
            dataType: 'json',
            data: {
                id: userId
            }
        }).done(function (respones) {
            if (respones.code){
                // $(".error-message").text(respones.message);
                // $(".error-message").fadeIn();
                alert(respones.message);
            } else {
                alert("Заявка успешно отправлена!");
                location.href = "/profile/" + userId;
            }
        })
    })
});