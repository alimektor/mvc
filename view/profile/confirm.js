$(document).ready(function(){
    $(document).on("click",".confirm-friend", function(){
        var requestId = $(this).closest(".request-friend").data("id");
        $.ajax({
            url: '/profile/ajaxConfirm/',
            type: 'POST',
            dataType: 'json',
            data: {
                id: requestId
            }
        }).done(function (respones) {
            if (respones.code){
                // $(".error-message").text(respones.message);
                // $(".error-message").fadeIn();
                alert(respones.message);
            } else {
                alert("Заявка принята!");
                location.href = "/profile/";
            }
        })
    });
});