/**
 * Created by Artem-PC on 28.06.2017.
 */
$(document).ready(function () {
    $(".change").click(function () {
        var status = $(".status").val();
        var vk = $(".vk").val();
        var twitter = $(".twitter").val();
        var skype = $(".skype").val();

        $.ajax({
            url: '/profile/InformationAJAX/',
            type: 'POST',
            dataType: 'json',
            data: {
                status: status,
                vk: vk,
                twitter: twitter,
                skype: skype
            }
        }).done(function (response) {
            if (response.code) { // Если ошибка
                $(".error-message").text(response.message);
                $(".error-message").fadeIn();
            } else { // если ошибки нет
                location.href = "/profile/";
            }
        });
    })
});