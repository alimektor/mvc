<?php
$page = \System\Page::instance();
$oInf = $page->getData("information");
use Blog\Information;
?>
<div id="myModalEdit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Редактирование профиля</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">

                <div class="form-group">
                    <label class="control-label" for="location">Статус:</label class="control-label">
                        <input type="text" class="form-control status" id="location"
                               placeholder="Введите свой статус" value="<?= $oInf->getTextStatus(); ?>">
                </div>
                <div class="form-group">
                    <label class="control-label" for="location">ID ВКонтакте:</label class="control-label">
                    <input type="text" class="form-control vk" id="location"
                           placeholder="Введите свой ID ВКонтакте" value="<?= $oInf->getVk() ?>">
                </div>
                <div class="form-group">
                    <label class="control-label" for="location">Логин Twitter:</label class="control-label">
                    <input type="text" class="form-control twitter" id="location"
                           placeholder="Введите свой логин в Twitter" value="<?= $oInf->getTwitter() ?>">
                </div>
                <div class="form-group">
                    <label class="control-label" for="location">Логин Skype:</label class="control-label">
                    <input type="text" class="form-control skype" id="location"
                           placeholder="Введите свой логин в Skype" value="<?= $oInf->getSkype()?>">
                </div>
            </div>

            <!-- Футер модального окна -->
            <div class="modal-footer">
<!--                <form action="/delete">-->
                    <button style="float: left" class="btn btn-danger del" data-toggle="modal" href="#myModalBoxExit">Удалить профиль</button>
<!--                </form>-->
                <div>
                    <button type="button" class="btn btn-default " data-dismiss="modal" aria-hidden="true"">Отмена</button>
                    <button type="submit" class="btn  btn-primary change" id="change">Изменить</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".mod").click(function () {
            $("#myModalEdit").modal('show');
        })
    });
</script>
<div id="myModalBoxExit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Удаление профиля</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                Вы уверены, что хотите удалить профиль?
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <form action="/delete" method="post">
                    <button type="button" class="btn btn-default " data-dismiss="modal" aria-hidden="true"">Отмена</button>
                    <button style="float: left" class="btn btn-danger del" data-toggle="modal" >Удалить профиль</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".del").click(function () {
            $("#myModalBoxExit").modal('show');
            $("#myModalEdit").modal('hide');
        })
    });
</script>
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="center">
                <img class="img-circle" src="https://pp.userapi.com/c624330/v624330001/58842/qGYJ-uFtgdk.jpg"
                     alt="Фото профиля">
            </div>
            <? if ($page->getFlag("user-btn") == false): ?>
                <div class="btn btn-default button-profile add-friends">
                    Добавить в друзья
                </div>
            <input id="userId" type="hidden" value="<?= $_GET["id"]; ?>">
                <div class="btn btn-default button-profile del-friends">
                    Удалить из друзей
                </div>
            <? else: ?>
            <div class="btn btn-default button-profile mod" href="#myModalEdit"  data-toggle="modal">
                Редактировать профиль
            </div>
            <? endif; ?>
        </div>
    </div>
    <div class="col-md-8">
        <h3>
            <?= $page->getData("username") ?>
        </h3>
        <div class="row">
            <div class="profile-info-left">
                <i class="fa fa-location-arrow" aria-hidden="true"></i> Местоположение:
            </div>
            <div class="profile-info-right">
                ??????
            </div>
            <div class="profile-info-left">
                <i class="fa fa-quote-left" aria-hidden="true"></i> Статус:
            </div>
            <div class="profile-info-right">
                <?= $oInf->getTextStatus(); ?>
            </div>
            <div class="profile-info-left">
                <i class="fa fa-vk" aria-hidden="true"></i> VK:
            </div>
            <div class="profile-info-right">
                <a href="https://vk.com/<?= $oInf->getVk() ?>" ><?= $oInf->getVk() ?></a>
            </div>
            <div class="profile-info-left">
                <i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter:
            </div>
            <div class="profile-info-right">
                <a href="https://twitter.com/<?= $oInf->getTwitter()?>"><?= $oInf->getTwitter()?></a>
            </div>
            <div class="profile-info-left">
                <i class="fa fa-skype" aria-hidden="true"></i> Skype:
            </div>
            <div class="profile-info-right">
                <a href="skype: <?= $oInf->getSkype()?>"> <?= $oInf->getSkype()?></a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tabbable" id="index_tabbable">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#notes" data-toggle="tab">Записи</a>
                </li>
                <li>
                    <a href="#friends" data-toggle="tab">Друзья</a>
                </li>
                <li>
                    <a href="#requests" data-toggle="tab">Заявки</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="notes">
                    <div class="row">
                        <?php foreach ($page->getData("notes") as $oNotes) : ?>
                            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                <p>Автор: <?= $oNotes->getUser()->getFullName() ?></p>
                                <p>
                                    Текст: <?= $oNotes->getNote() ?>
                                </p>
                                <p>
                                    Время: <?= $oNotes->getTime() ?>
                                </p>
                                <hr>
                            </div>
                            <div class="alert alert-danger error-message" style="display: none;" role="alert"></div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="tab-pane" id="friends">
                    <?php foreach ($page->getData("friends") as $oFriend): ?>
                    <div class="row">
                        <div class="col-md-2">
                            <img class="img-circle img-friends"
                                 src="https://pp.userapi.com/c624330/v624330001/58842/qGYJ-uFtgdk.jpg"
                                 alt="Friend">
                        </div>
                        <div class="col-md-10">
                            <div class="friend-info-name">
                                <a href="/profile/<?= $oFriend->getId() ?>"><?= $oFriend->getFullName() ?></a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="tab-pane" id="requests">
                    <?php foreach ($page->getData("requests") as $oRequest): ?>
                        <div class="row request-friend" data-id="<?=$oRequest->getId()?>">
                            <div class="col-md-2">
                                <img class="img-circle img-friends"
                                     src="https://pp.userapi.com/c624330/v624330001/58842/qGYJ-uFtgdk.jpg"
                                     alt="Friend">
                            </div>
                            <div class="col-md-10">
                                <div class="friend-info-name">
                                    <a href="/profile/<?= $oRequest->getId() ?>"><?= $oRequest->getFullName() ?></a>
                                    <? if ($page->getFlag("user-btn") == true): ?>
                                            <button class="btn btn-link confirm-friend">Принять</button>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>