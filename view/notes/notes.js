/**
 * Created by Artem-PC on 26.06.2017.
 */
$(document).ready(function () {
    $(".send").click(function () {
        var notes = $("#note").val();
        $.ajax({
            url: 'notes/ajaxNotes/',
            type: 'POST',
            dataType: 'json',
            data: {
                notes: notes
            }
        }).done(function (respones) {
            if (respones.code){
                $(".error-message").text(respones.message);
                $(".error-message").fadeIn();
            } else {
                location.href = "notes";
            }
        })
    })
});