<?php
$page = \System\Page::instance();
/* @var $oNotes \Blog\Note */
?>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="form-horizontal">
            <div class="form-group">
                <label for="note"></label>
                <input class="form-control" id="note" name="note" type="text" placeholder="Текст записи">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block send">Отправить запись</button>
            </div>
        </div>
    </div>
    <?php foreach ($page->getData("notes") as $oNotes) : ?>
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <p>Автор: <?= $oNotes->getUser()->getFullName() ?></p>
        <p>
            Текст: <?= $oNotes->getNote() ?>
        </p>
        <p>
            Время: <?= $oNotes->getTime() ?>
        </p>
    </div>
        <div class="alert alert-danger error-message" style="display: none;" role="alert"></div>
    <?php endforeach; ?>
</div>