<? 
System\Page::instance()->setTitle("404 Страница не найдена");
System\Page::instance()->addStyle("/view/templates/landing/css/errors.min.css");
?>
<div class="error">
      <div class="error-body">
        <h1 class="error-heading">Страница не найдена</h1>
        <h4 class="error-subheading">К сожалению, запрашиваемая Вами страница не найдена.</h4>
        <p>
          <small>Возможно вы ввели неверный URL или страница, которую вы ищете больше недоступна.</small>
        </p>
        <p><a class="btn btn-primary btn-pill btn-thick" href="/">Перейти на главную</a></p>
      </div>
      <div class="error-footer">
        <!--ul class="list-inline">
          <li>
            <a class="link-muted" href="#">
              <span class="icon icon-twitter icon-2x"></span>
            </a>
          </li>
          <li>
            <a class="link-muted" href="#">
              <span class="icon icon-facebook-official icon-2x"></span>
            </a>
          </li>
          <li>
            <a class="link-muted" href="#">
              <span class="icon icon-linkedin icon-2x"></span>
            </a>
          </li>
        </ul-->
        <p>
          <small>© 2016 ATVshniki</small>
        </p>
      </div>
    </div>
