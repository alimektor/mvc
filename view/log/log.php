<?php
$page = System\Page::instance();
?>

<div class="row">
    <div class="col-md-12">
        <h2>Авторизация</h2>
        <div class="form">
            <div class="alert alert-danger error-message" style="display: none;" role="alert"></div>
            <div class="form-group">
                <input type="email" class="form-control" id="InputEmailLog" placeholder="Адрес электронной почты">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="InputPasswordLog" placeholder="Пароль">
                <div class="help-block"><a href="#">Забыли пароль?</a></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block send">Войти</button>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox">
                    Запомнить меня
                </label>
            </div>
        </div>
        <div class="bottom text-right">
            Впервые на сайте?
            <a href="/reg"><b>Регистрация</b></a>
        </div>
    </div>
</div>

