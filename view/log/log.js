$(document).ready(function () {
    $(".send").click(function () {
        var userEmail = $("#InputEmailLog").val();
        var userPassword = $("#InputPasswordLog").val();
        // Отправляем запрос серверу
        $.ajax({
            url: 'log/ajaxLog/',
            type: 'POST',
            dataType: 'json',
            data: {
                email: userEmail,
                password: userPassword
            }
        }).done(function (response) {
            // response - это по-сути $arResponse из контроллера

            if (response.code) { // Если ошибка
                $(".error-message").text(response.message);
                $(".error-message").fadeIn();
            } else { // если ошибки нет
                location.href = "/";
            }
        });
    });

});