﻿<?
$page = System\Page::instance();
use Blog\User;
?>
<!DOCTYPE html>
<html>
<head>
	<title><?= $page -> getTitle() ?> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="/view/templates/default/css/bootstrap.css" rel="stylesheet">
	<link href="/view/templates/default/css/font-awesome.css" rel="stylesheet">
    <? foreach ($page->getStyles() as $style): ?>
    <link href="<?= $style ?>" rel="stylesheet">
    <? endforeach; ?>
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
    <? foreach ($page->getHeadScripts() as $headScript): ?>
    <script src="<?= $headScript ?>"></script>
    <? endforeach; ?>
</head>
<body>
<div id="myModalBox" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Выход из сети</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                Вы уверены, что хотите выйти?
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <form action="/exit" method="post">
                <button type="button" class="btn btn-default " data-dismiss="modal" aria-hidden="true"">Отмена</button>
                    <button type="submit" class="btn  btn-primary">Выйти</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <!-- NAVIGATION START -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button> <a class="navbar-brand" href="#">Типа лого</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/">Главная</a>
                </li>
                <li>
                    <a href="/about">О нас</a>
                </li>
                <li>
                    <a href="/help">Помощь</a>
                </li>
            </ul>
            <form class="navbar-form navbar-right form-search" role="search">
                <div class="input-group">
                    <div class="form-group">
                        <input type="text" class="form-control input-medium search-query" placeholder="Поиск по сайту">
                    </div>
                    <span class="input-group-btn">
						<button type="submit" class="btn btn-default">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
						</span>
                </div>

            </form>
            <?php
            if (empty($_SESSION["userID"])):
            ?>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/log">Авторизация</a>
                </li>
                <li>
                    <a href="/reg">Регистрация</a>
                </li>
            </ul>
            <?php  else : ?>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/profile">Мой профиль</a></li>
                <li><a href="/notes">Мой блог</a></li>
                <li><a href="#myModalBox" class="mod" data-toggle="modal">Выход</a></li>
            </ul>
            <? endif;  ?>
        </div>
    </nav>

    <!-- NAVIGATION END -->
    <div style="margin-top: 100px"></div>
    <?php
    echo $page -> getContent();
    ?>
<!-- FOOTER START -->
	<footer>
		<div class="row">
			<div class="col-md-4">
				<h3>Sitemap</h3>
						<a href="#">Home</a>
						<a href="#">About</a>
						<a href="#">Services</a>
			</div>
			<div class="col-md-4">
				<h3>Sitemap</h3>
						<a href="#">Home</a>
						<a href="#">About</a>
						<a href="#">Services</a>
			</div>
			<div class="col-md-4">
				<h3>Copyright</h3>
					<p>
						Alimektor © 2017
					</p>
			</div>
		</div>
	</footer>
<!-- FOOTER END -->
</div>
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="/view/templates/default/js/bootstrap.js"></script>
    <? foreach ($page->getFooterScripts() as $footerScript): ?>
        <script src="<?= $footerScript ?>"></script>
    <? endforeach; ?>
</body>
</html>