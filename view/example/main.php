<?php System\Page::instance()->addFooterScript("script.js"); ?>

<div class="row" style="padding-top: 150px;">
    <div class="col-lg-4">
        <div class="form-group">
            <input type="text" class="form-control user-login" placeholder="Логин">
        </div>
        <div class="form-group">
            <input type="password" class="form-control user-password" placeholder="Пароль">
        </div>
        <div class="form-group">
            <button class="btn btn-success send">Отправить</button>
        </div>
    </div>
</div>

<div class="alert alert-danger error-message" style="display: none;" role="alert"></div>
<div class="alert alert-success success-message" style="display: none;" role="alert"></div>