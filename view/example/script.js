$(document).ready(function () {

    /**
     * Событие при нажатии на кнопку "Отправить"
     */
    $(".send").click(function () {
        var userLogin = $(".user-login").val(); // Получаем логин из инпута
        var userPassword = $(".user-password").val(); // Получаем пароль из другого инпута
        // Отправляем запрос серверу
        $.ajax({
            url: '/example/ajaxExample/',
            type: 'POST',
            dataType: 'json',
            data: {
                login: userLogin,
                password: userPassword
            }
        }).done(function(response) {
            // response - это по-сути $arResponse из контроллера
            if (response.code) { // Если ошибка
                console.log("error");
                $(".alert-danger").text(response.message);
                $(".alert-success").hide();
                $(".alert-danger").fadeIn();
            } else { // если ошибки нет
                console.log("Success!");
                $(".alert-success").text(response.message);
                $(".alert-danger").hide();
                $(".alert-success").fadeIn();
            }
        });
    });

});