<?php
$page = System\Page::instance();
?>
<div class="row">
    <div class="col-md-12">
        <h2>Регистрация</h2>
        <div class="alert alert-danger error-message" style="display: none;" role="alert"></div>
        <div class="form-horizontal">

            <div class="form-group">
                <label class="control-label col-xs-3" for="FirstName">Имя:</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="FirstNameRegistration" placeholder="Введите имя">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="LastName">Фамилия:</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="LastNameRegistration" placeholder="Введите фамилию">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-xs-3" for="InputEmailReg">E-mail:</label>
                <div class="col-xs-9">
                    <input type="email" class="form-control" id="InputEmailRegistration"
                           placeholder="Введите Ваш адрес электронной почты">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="InputPasswordReg">Пароль:</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="InputPasswordRegistration"
                           placeholder="Введите пароль">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="ConfirmPassword">Подтвердите пароль:</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="ConfirmPasswordRegistration"
                           placeholder="Введите пароль ещё раз">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="submit" class="btn btn-primary send" value="Регистрация">
                </div>
            </div>
        </div>
    </div>
</div>
