$(document).ready(function () {
    $(".send").click(function () {
        var userSurname = $("#LastNameRegistration").val();
        var userName = $("#FirstNameRegistration").val();
        var userEmail = $("#InputEmailRegistration").val();
        var userPassword = $("#InputPasswordRegistration").val();
        var userConfirm = $("#ConfirmPasswordRegistration").val();
        // Отправляем запрос серверу
        $.ajax({
            url: 'reg/ajaxReg/',
            type: 'POST',
            dataType: 'json',
            data: {
                name: userName,
                surname: userSurname,
                email: userEmail,
                password: userPassword,
                confirmPassword: userConfirm
            }
        }).done(function (response) {
            // response - это по-сути $arResponse из контроллера

            if (response.code) { // Если ошибка
                $(".error-message").text(response.message);
                $(".error-message").fadeIn();
            } else { // если ошибки нет
                location.href = "/";
            }
        });
    });

});