<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/system/init.php"; //Подключения ядра системы
use System\User;
$oUser = User::current();
if($_SERVER["REMOTE_ADDR"] != "127.0.0.1" && $oUser->getId() != 1 && $_SERVER["REMOTE_ADDR"] != "62.148.138.4"){ //Проверка доступа к выполнению миграций
        die("<h1>Вы выполняете миграции не локально. Админ, залогинься!</h1>");
}


global $DBi;
include './lib/Migration.php';
$migration = new Migration($DBi, __DIR__ . "/sql");

if (@$_REQUEST["upgrade"] == "true") {
	try {
		$migration->upgrade($arJustAppliedVersions);
	} catch (Exception $exc) {
		$message = $exc->getMessage();
	}
}
?>
<html>
    <head>
		<meta charset="utf-8"/>
        <title>Миграции базы данных</title>
		<style>
			body {
				padding: 20px 100px;
			}

			ul {
				width: 350px;
				overflow: scroll;
				height: 600px;
			}
			.column {
				display: inline-block;
				padding-right: 50px;
				vertical-align: top;
			}
			.message{
				width: 500px;
			}
			.just-applied-list {
				color: green;
			}
		</style>
    </head>
    <body>
		<div class="column">
			<h2>Примененные миграции</h2>
			<ul>
				<?php foreach ($migration->getVersions("DESC") as $version): ?>
					<li><?php echo $version ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php if (!empty($migration->getDeltas())): ?>
			<div class="column">
				<h2>Неприменненные миграции</h2>
				<ul>
					<?php foreach ($migration->getDeltas() as $delta): ?>
						<li><?php echo Migration::getVersionFromFilename($delta) ?></li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
		<div class="column message">
			<?php if (@$_REQUEST["upgrade"] == "true") {
				echo @$message;
			} ?>
				<?php if (!empty($arJustAppliedVersions)): ?>
				<h2>К базе данных применены новые миграции</h2>
				<ul class="just-applied-list">
					<?php foreach ($arJustAppliedVersions as $version): ?>
						<li><?php echo $version ?></li>
				<?php endforeach; ?>
				</ul>
                <?php endif; ?>
		</div>
		<form action="" method="POST">
			<div class="manage">
				<?php if (empty($migration->getDeltas())): ?>
					<h3>Нет непримененных (новых) миграций</h3>
                <?php endif; ?>

				<input type="hidden" name="upgrade" value="true">
				<input type="submit" value="Применить новые миграции">
			</div>
		</form>
    </body>
</html>
