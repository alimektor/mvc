<?php

/**
 * Класс, реализующий миграции базы данных.
 * Основан на методе базового состояния БД и последовательного
 * применения к ней изменений (миграций).
 *
 * @author Николай Колосов <nk@webberry.ru>
 * @version 1.0.2-atv
 */
class Migration {
	
	/* @var $mysqli \mysqli */
	private $mysqli;

	/**
	 * Путь до директории с миграциями.
	 * @var string
	 */
	private $path;

	const BASE_VERSION = "0000000_0000";
	const DB_VERSION_TABLE = "db_version";

	/**
	 * Создать объект инструмента миграции 
	 * @param \mysqli $mysqli объект mysql (подключенный к нужной БД)
	 * @param string $path Путь к директории с миграциями
	 */
	public function __construct($mysqli, $path) {
		$this->mysqli = $mysqli;
		$this->path = $path;
	}

	/**
	 * Применялась ли данная версия к БД
	 * @param string $version
	 * @return boolean
	 * @throws \Exception
	 */
	private function isNewVersion($version) {
		$sql = "SELECT count(*) cnt FROM " . self::DB_VERSION_TABLE . " WHERE version ='" . $this->mysqli->real_escape_string($version) . "'";
		$oResult = $this->mysqli->query($sql);
		if (!$oResult) {
			throw new \Exception("Невозможно выполнить завпрос с БД для проверки текущей версии");
		}
		$count = (int) $oResult->fetch_object()->cnt;

		// Уже применено
		if ($count)
			return false;

		return true;
	}

	/**
	 * Получить текущую версию (версию последней миграции) базы данных
	 * @return string
	 * @throws \Exception
	 */
	public function getDatabaseVersion() {
		$sql = "SELECT version FROM " . self::DB_VERSION_TABLE . " ORDER BY version DESC LIMIT 1";

		/* @var $oResult \mysqli_result */
		$oResult = $this->mysqli->query($sql);

		if (!$oResult)
			throw new \Exception("Невозможно выполнить запрос на получение версии базы данных");

		if (!$oResult->num_rows)
			return self::BASE_VERSION;

		return $oResult->fetch_object()->version;
	}

	/**
	 * Получить массив версий, которые не применялись к текущей базе данных
	 * @return array
	 */
	function getDeltas() {
		// Получить массив файлов с расширением sql в директории с миграциями
		$arAllDeltas = glob($this->path . '/*.{sql}', GLOB_BRACE);
		// Массив с новыми, не применнеными миграциями
		$arNewDeltas = [];
		foreach ($arAllDeltas as $delta) {
			$deltaVersion = self::getVersionFromFilename($delta);
			if ($this->isNewVersion($deltaVersion)) {
				$arNewDeltas[] = $delta;
			}
		}
		return $arNewDeltas;
	}

	/**
	 * Получить версию миграции, содержащуюся в имени файла.
	 * Например, имя файла 20161123_1505_nk.sql => версия 20161123_1505_nk
	 * @param string $fileName
	 * @return string
	 */
	public static function getVersionFromFilename($fileName) {
		// Убираем путь до файла, оставляем только его имя
		$result = basename($fileName);
		// Убираем расширение файла
		$result = str_replace(".sql", "", $result);
		return $result;
	}

	/**
	 * Обновить базу данных до последней версии
	 * @param array $arVersions - переменная (массив) куда записываются примененные версии (передается по ссылке)
	 * @throws \Exception
	 */
	public function upgrade(&$arVersions) {
		$arVersions = [];
		foreach ($this->getDeltas() as $delta) {
			$deltaContentent = file_get_contents($delta);
			$deltaVersion = self::getVersionFromFilename($delta);
			$updateVersionSQL = "INSERT INTO " . self::DB_VERSION_TABLE . " (version) VALUES ('" . $this->mysqli->real_escape_string($deltaVersion) . "');";
			$sql = "set autocommit = 0; START TRANSACTION; $deltaContentent $updateVersionSQL COMMIT;";

			$this->mysqli->multi_query($sql);
			while ($this->mysqli->more_results() && $this->mysqli->next_result())
				$this->mysqli->store_result();

			if ($this->mysqli->error)
				throw new \Exception("Невозможно обновиться на версию: $deltaVersion. " . $this->mysqli->errno . " " . $this->mysqli->error);

			$arVersions[] = $deltaVersion;
		}
	}

	/**
	 * Получить версии (миграции) примененные к БД
	 * @param string $sort
	 * @return array
	 */
	public function getVersions($sort = "ASC") {
		$arVersions = [];
		if ($sort === "ASC") {
			$sortSql = "ASC";
		} else {
			$sortSql = "DESC";
		}
		$oResult = $this->mysqli->query("SELECT version FROM " . self::DB_VERSION_TABLE . " ORDER BY version " . $sortSql);
		if ($oResult) {
			while ($oRow = $oResult->fetch_object()) {
				$arVersions[] = $oRow->version;
			}
		}
		return $arVersions;
	}

}
