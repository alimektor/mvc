--
-- Структура таблицы `db_version`
--

CREATE TABLE `db_version` (
  `version` varchar(50) NOT NULL COMMENT 'версия базы данных',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'время обновления до указанной версии'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы таблицы `db_version`
--
ALTER TABLE `db_version`
  ADD PRIMARY KEY (`version`);

