<?php $title = "Работа с классами"; ?>
<? require_once './template/header.php'; ?>
<h1>Работа с классами</h1>
<p>
	Классы располагать в <code>/model/</code>. Расположение красса внутри данной директории должно соответствовать namespace'у.<br>
	Например класс <code>\Atv\Auto\Car</code> должен лежать в <code>/model/Atv/Auto/Car.php</code>.
</p>
<p>
	Название классов и namespace'ов должно быть в формате <code>CamelCase</code> - с большой буквы.<br>
	Название всех методов и свойств внутри класса должно быть в формате <code>camelCase</code> - с маленькой буквы
</p>
<p>
	К каждому свойству и методу должны быть комментарии в формате <a href="https://www.phpdoc.org/docs/latest/index.html" target="blank">phpdoc</a>.
</p>
<p>
	Пример:
</p>
<div class="code">	namespace System;

	/**
	* Класс для работы с пользователями.
	*
	* @author Николай Колосов <nk@webberry.ru>
	* @version 1.0.0-atv
	*/
	
	class User {
	
		/**
		* Отчество пользователя
		* @var string
		*/
		protected $patronymic;

		/**
		* Дата Последней активности
		* @var \DateTime
		*/
		protected $lastActivity; 
		
		/**
		* Получить дату и время создания пользователя
		* @return \DateTime Дата и время создания пользователя
		*/
		function getCreated() {
			if (!$this->created){
				$this->created = new \DateTime("1970-01-01");
			}
			
			return $this->created;
		}
		
		/**
		 * Задать логин пользователя
		 * @param string $login Логин пользователя
		 * @throws \Exception
		 */
		function setLogin($login) {
			self::checkLogin($login);
			$this->login = $login;
		}
	}
</div>
<? require_once './template/footer.php';
?>