<? //if ($_SERVER["REMOTE_ADDR"] !== "127.0.0.1") die(); ?>
<html>
    <head>
        <title><?= $title ?></title>
		<link href="/tools/doc/template/style.css" rel="stylesheet" type="text/css"/>
		<link href="/tools/doc/template/styles/idea.css" rel="stylesheet" type="text/css"/>
		<script src="/tools/doc/template/highlight.pack.js" type="text/javascript"></script>
		<script src="/tools/doc/template/jquery-3.1.1.min.js" type="text/javascript"></script>
		<script>
			$(document).ready(function () {
				$('div.code').each(function (i, block) {
					hljs.highlightBlock(block);
				});
			});
		</script>
    </head>
    <body>
		<div class="menu">
			<span>Разделы</span>
			<ul>
				<li><a href="pages.php">Создание страниц</a></li>
				<li><a href="classes.php">Классы PHP</a></li>
				<li><a href="blocks.php">Блоки</a></li>
				<!--<li><a href="model_db.php">Работа с БД в классах</a></li>-->
<!--				<li>Классы
					<ul>
						<li><a href="bloks.php">Page</a></li>
						<li><a href="bloks.php">User</a></li>
						<li><a href="bloks.php">SQL</a></li>
					</ul>
				</li>-->
			</ul>
		</div>
		<div class="content">

