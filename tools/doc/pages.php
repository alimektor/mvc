<?php $title = "Страницы"; ?>
<? require_once './template/header.php'; ?>
<h1>MVC и как создать страницу</h1>
<p>
	Сайт состоит из страниц. 
	Проект разделен model, view, control.
	Итак, как создать очередную страницу на сайте.
</p>
<h2>Control</h2>
<p>
	В проекте используется единая точка входа - <code>/index.php</code><br>
	Все запросы, кроме js, css изображений и т.д., проходят через /index.php и далее запускается контроллер, соответствующий url адресу.<br>
	Например: <code>http://site.com/example/</code> запустит выполнение скрипта <code>/control/example/index.php</code>.<br>
	Параметры можно передавать как с помощью простого GET запроса <code>http://site.com/example/?id=value&name=test</code>, <br>
	так и в человеко понятном виде. Для этого надо в htaccess указать следующее правило:
</p>
<div class="code">RewriteRule ^users/([0-9]*)/?$ /index.php?path=users&id=$1 [L,QSA]</div>
<p>
	Таким образом <code>http://site.com/users/21/</code> запустит скрипт <code>/control/users/index.php</code> и в <code>$_GET["id"]</code>
	будет лежать 21
</p>
<p>
	<span class="warning">Примечание:</span> В контроллере не закрывать тег <code>&lt;&#063;php</code> чтобы не передавать лишних символов в виде пустых строк и пробелов
</p>
<h2>View</h2>
<p>
	После того, как в контроллере произведен всего того, что нужно - можно подключить шаблон страницы (если это нужно).
</p>
<p>
	Делается это так:
</p>
<div class="code">\System\Page::instance()->display("example/main.php","mytemplate");</div>
<p>
	Данный код подключит представление - шаблон который лежит в <code>/view/example/main.php</code><br> с шаблоном mytemplate, (по умолчанию "default")
	<code>\System\Page::instance()</code> - это объект web страницы. Данный объект имеет методы setTitle, addBlock, addHeadScript, addFooterScript, addStyle, setContent, display.<br>
	С помощью которых можно задать заголовок страницы, добавить на страницу <a href="/tools/doc/blocks.php">блок</a>, отобразить страницу.
</p>
<p>
	Код представления (Шаблона) может выглядеть так:
</p>
<div class="code">	&lt;&#063;
	global $arUsers; // Массив объектов, сформированный в контроллере, который можем вывести
	// Подключаем стили и скрипт, лежащие в той же папке, что и main.php
	\System\Page::instance()-&gt;addStyle("style.css");
	\System\Page::instance()-&gt;addHeadScript("script.js");
	// Задаем заголовок страницы
	\System\Page::instance()-&gt;setTitle("Список пользователей");
	&#063;&gt;
	&lt;h1&gt;Список пользователей&lt;/h1&gt;
	&lt;&#063; // Пример подключения блоков к странице &#063;&gt;
	&lt;&#063; \System\Page::instance()-&gt;addBlock("Examples/Users", ["example" =&gt; true]); &#063;&gt;
	&lt;&#063; \System\Page::instance()-&gt;addBlock("Examples/Users", ["hello" =&gt; "world"],"default"); &#063;&gt;
</div>
<? require_once './template/footer.php'; ?>